﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Arefjev.Bell.Storage.BL.Model;
using Arefjev.Bell.Storage.Dal.Sql.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Arefjev.Bell.Storage.Dal.Sql.Test
{
	/// <summary>
	/// Тесты для проверки работы БД на основе Entity Framework.
	/// </summary>
	[TestClass]
	public class StoreContextTests
	{
		//const string DatabaseName = "StoreTest";
		//const string ConnectionString = @"Server=DENIS-HP;Database=StoreTest;Integrated Security = True;MultipleActiveResultSets = True";

		/// <summary>
		/// Инициалиирует БД.
		/// </summary>
		/// <param name="context"></param>
		[ClassInitialize]
		public static void Initialize(TestContext context)
		{
			Database.SetInitializer(new DropCreateDatabaseIfModelChanges<StoreContext>());
		}

		/// <summary>
		/// Инициализация перед выполнением каждого теста.
		/// Очищает основные таблицы.
		/// </summary>
		[TestInitialize]
		public void InitializeTest()
		{
			ClearShopAssistents();
			ClearGoods();
		}

		/// <summary>
		/// Проверяет инициализацию БД.
		/// </summary>
		[TestMethod]
		public void CreateShopDbTest()
		{
			using (var ctx = CreateDbContext())
			{
				ctx.Database.Initialize(false);
				ctx.Assistents.ToList();
			}
		}

		/// <summary>
		/// Проверяет добавление продавцов.
		/// </summary>
		[TestMethod]
		public void CreateShopAssistentsTest()
		{
			ShopAssistent assistent;

			using (var ctx = CreateDbContext())
			{
				assistent = CreateShopAssistent();
				ctx.Assistents.Add(assistent);
				ctx.SaveChanges();
			}

			using (var ctx = CreateDbContext())
			{
				var exists = ctx.Assistents.Any(a => a.FullName == assistent.FullName);

				Assert.IsTrue(exists);
			}
		}

		/// <summary>
		/// Проверяет добавление чеков.
		/// </summary>
		[TestMethod]
		public void CreateChecksTest()
		{
			ShopAssistent assistent;
			Check check;
			List<Goods> goods;
			DateTime sellDate;

			using (var ctx = CreateDbContext())
			{
				assistent = CreateShopAssistent();
				ctx.Assistents.Add(assistent);

				goods = new List<Goods>(CreateGoods());
				goods.ForEach(g => ctx.Goods.Add(g));

				check = new Check { ShopAssistent = assistent };
				check.Items.AddRange(goods.Select(g => new CheckItem { AmountOfGoods = 1m, Goods = g }));

				sellDate = check.SellDate;

				ctx.Checks.Add(check);
				ctx.SaveChanges();
			}

			using (var ctx = CreateDbContext())
			{
				var sell = ctx.Checks
					.Any(c => c.ShopAssistent.Id == assistent.Id && c.SellDate == sellDate);

				Assert.IsTrue(sell);
			}
		}

		/// <summary>
		/// Проверяет добавление товаров.
		/// </summary>
		[TestMethod]
		public void CreateGoodsTest()
		{
			using (var ctx = CreateDbContext())
			{
				foreach (var goods in CreateGoods())
					ctx.Goods.Add(goods);

				ctx.SaveChanges();
			}

			using (var ctx = CreateDbContext())
			{
				var goods = ctx.Goods.ToList();
				var goods2 = new List<Goods>(CreateGoods());

				for (int i = 0, n = goods.Count; i < n; ++i)
				{
					Assert.AreEqual(goods[i].Name, goods2[i].Name);
					Assert.AreEqual(goods[i].Article, goods2[i].Article);
					Assert.AreEqual(goods[i].Price, goods2[i].Price);
					Assert.AreEqual(goods[i].Unit, goods2[i].Unit);
				}
			}
		}

		/// <summary>
		/// Проверяет поступление товаров.
		/// </summary>
		[TestMethod]
		public void GoodsIncomingTest()
		{
			using (var ctx = CreateDbContext())
			{
				foreach (var goods in CreateGoods())
					ctx.Goods.Add(goods);

				ctx.SaveChanges();
			}

			using (var ctx = CreateDbContext())
			{
				foreach (var goods in ctx.Goods)
				{
					var goodStore = new GoodsStore { Goods = goods, AmountOfGoods = 100 };

					ctx.GoodsStores.Add(goodStore);
				}

				ctx.SaveChanges();
			}

			using (var ctx = CreateDbContext())
			{
				var goods = ctx.Goods.ToList();

				foreach (var goodStore in ctx.GoodsStores)
				{
					Assert.IsTrue(goods.Any(g => g.Id == goodStore.Goods.Id));
				}
			}
        }

		/// <summary>
		/// Создает товары.
		/// </summary>
		/// <returns>Перечисление товаров.</returns>
		private static IEnumerable<Goods> CreateGoods()
		{
			for (int i = 0, n = 5; i < n; ++i)
			{
				yield return new Goods
				{
					Article = string.Format("ART-{0:000}", i + 1),
					Name = string.Format("Изделие № {0}", i),
					Price = 100 * i + 500
				};
			}
		}

		/// <summary>
		/// Создает тестового продавца.
		/// </summary>
		/// <returns></returns>
		private static ShopAssistent CreateShopAssistent()
		{
			return new ShopAssistent { FullName = "Петров Иван Иванович", PassportId = "00-0000000" };
		}

		/// <summary>
		/// Создвет контекст БД.
		/// </summary>
		/// <returns>Контекст.</returns>
		private static StoreContext CreateDbContext()
		{
			var dbCtx = new StoreContext("StoreTestConnection");

			//dbCtx.Database.Connection.ConnectionString = ConnectionString;

			return dbCtx;
		}

		/// <summary>
		/// Удаляет всех продавцов.
		/// </summary>
		private static void ClearShopAssistents()
		{
			using (var ctx = CreateDbContext())
			{
				var assistents = ctx.Assistents.ToList();
				assistents.ForEach(a => ctx.Assistents.Remove(a));

				ctx.SaveChanges();
            }
		}

		/// <summary>
		/// Удаляет все товары.
		/// </summary>
		private static void ClearGoods()
		{
			using (var ctx = CreateDbContext())
			{
				var assistents = ctx.Goods.ToList();
				assistents.ForEach(g => ctx.Goods.Remove(g));

				ctx.SaveChanges();
			}
		}
	}
}

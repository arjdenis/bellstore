﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Arefjev.Bell.Storage.BL;
using Arefjev.Bell.Storage.BL.Model;
using Arefjev.Bell.Storage.BL.View;

namespace Arefjev.Bell.UI.WinForms
{
	/// <summary>
	/// Редактор товара.
	/// </summary>
	public partial class GoodsEditForm : Form, IGoodsEditView
	{
		/// <summary>
		/// Инициализирует новый экземпляр.
		/// </summary>
		/// <param name="goods">Модель товара.</param>
		public GoodsEditForm(Goods goods)
		{
			Goods = goods ?? new Goods();

			InitializeComponent();

			_numericUpDownPrice.Minimum = decimal.MinValue;
			_numericUpDownPrice.Maximum = decimal.MaxValue;
		}

		/// <summary>
		/// Возвращает/устанавливает модель товара.
		/// </summary>
		public Goods Goods { get; set; }

		/// <summary>
		/// Вызывается при загрузке.
		/// Привязка к данным.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void GoodsEditForm_Load(object sender, EventArgs e)
		{
			_textBoxArt.DataBindings.Add("Text", Goods, "Article");
			_textBoxName.DataBindings.Add("Text", Goods, "Name");
			_textBoxUnit.DataBindings.Add("Text", Goods, "Unit");
			_numericUpDownPrice.DataBindings.Add("Value", Goods, "Price");
		}

		/// <summary>
		/// Вызывается при валидации поля артикула.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnArt_Validating(object sender, CancelEventArgs e)
		{
			_errorProvider.SetError(_textBoxArt, null);
            if (string.IsNullOrEmpty(_textBoxArt.Text))
			{
				_errorProvider.SetError(_textBoxArt, "Article is not set!");
				e.Cancel = true;
			}
		}

		/// <summary>
		/// Вызывается при валидации поля наименования.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnName_Validating(object sender, CancelEventArgs e)
		{
			_errorProvider.SetError(_textBoxName, null);
			if (string.IsNullOrEmpty(_textBoxName.Text))
			{
				_errorProvider.SetError(_textBoxName, "Name is not set!");
				e.Cancel = true;
			}
		}

		/// <summary>
		/// Открывает редактор в диалоговом режиме.
		/// </summary>
		/// <returns>true, если нажата кнопка ОК, иначе false.</returns>
		bool IView.ShowDialog()
		{
			return ShowDialog() == DialogResult.OK;
		}

		/// <summary>
		/// Вызывается при закрытии формы.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void GoodsEditForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (DialogResult == DialogResult.OK)
				if (ValidateChildren() == false)
				{
					e.Cancel = true;
				}
		}
	}
}

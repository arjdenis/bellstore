﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Arefjev.Bell.Storage.BL;
using Arefjev.Bell.Storage.BL.Model;
using Arefjev.Bell.Storage.BL.View;

namespace Arefjev.Bell.UI.WinForms
{
	/// <summary>
	/// Редактор продавцов.
	/// </summary>
	public partial class AssistentsForm : Form, IAssistentsView
	{
		/// <summary>
		/// Инициализирует новый экземпляр.
		/// </summary>
		public AssistentsForm()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Возвращает список выбранных продавцов.
		/// </summary>
		public IList<ShopAssistent> SelectedAssistents
		{
			get
			{
				return _dataGridView.SelectedRows
					.OfType<DataGridViewRow>()
					.Select(row => (ShopAssistent)row.DataBoundItem)
					.ToList();
			}
		}

		/// <summary>
		/// Команда Нанять продавца.
		/// </summary>
		public event EventHandler HireAssistentCommand = delegate { };

		/// <summary>
		/// Команда Уволить продавца.
		/// </summary>
		public event EventHandler DismitCommand = delegate { };

		/// <summary>
		/// Команда сохранить изменения.
		/// </summary>
		public event EventHandler SaveCommand = delegate { };

		/// <summary>
		/// Отображает список продавцов.
		/// </summary>
		/// <param name="assistents">Список продавцов.</param>
		public void LoadAssistents(IList<ShopAssistent> assistents)
		{
			_bindingSource.DataSource = assistents;
		}

		/// <summary>
		/// Вызывается при нажатии кнопки ОК.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnClose_Click(object sender, EventArgs e)
		{
			Close();
		}

		/// <summary>
		/// Вызывается при нажатии кнопки Нанять продавца.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnHire_Click(object sender, EventArgs e)
		{
			HireAssistentCommand(this, EventArgs.Empty);
		}

		/// <summary>
		/// Вызывается при нажатии кнопки Уволить продавца.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnDismit_Click(object sender, EventArgs e)
		{
			DismitCommand(this, EventArgs.Empty);
		}

		/// <summary>
		/// Вызывается при нажатии кнопки Сохранить.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnSave_Click(object sender, EventArgs e)
		{
			SaveCommand(this, EventArgs.Empty);
			Close();
		}

		bool IView.ShowDialog()
		{
			return ShowDialog() == DialogResult.OK;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Arefjev.Bell.Storage.BL;
using Arefjev.Bell.Storage.BL.Model;
using Arefjev.Bell.Storage.BL.View;

namespace Arefjev.Bell.UI.WinForms
{
	/// <summary>
	/// Редактор поступления товаров.
	/// </summary>
	public partial class GoodsIncomingForm : Form, IIncomingGoodsView
	{
		/// <summary>
		/// Инициализирует новый экземпляр.
		/// </summary>
		public GoodsIncomingForm()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Возвращает список поступлений на склад.
		/// </summary>
		public IEnumerable<GoodsStore> IncomingGoods
		{
			get
			{
				return _dataGridView.Rows
					.OfType<DataGridViewRow>()
					.Where(r => decimal.Parse(r.Cells[_columnAmount.Index].Value.ToString()) > 0m)
					.Select(r => new GoodsStore
					{
						Goods = (Goods)r.Tag,
						AmountOfGoods = decimal.Parse(r.Cells[_columnAmount.Index].Value.ToString())
					});
			}
		}

		/// <summary>
		/// Событие сохранить поступление товара.
		/// </summary>
		public event EventHandler SaveCommand = delegate { };

		/// <summary>
		/// Загружает список товаров.
		/// </summary>
		/// <param name="goods">Список товаров.</param>
		public void LoadGoods(IList<Goods> goods)
		{
			_dataGridView.Rows.Clear();

            foreach (var g in goods)
			{
				var index = _dataGridView.Rows.Add();
				var row = _dataGridView.Rows[index];

                row.Tag = g;
				row.Cells[_columnArticle.Index].Value = g.Article;
				row.Cells[_columnName.Index].Value = g.Name;
				row.Cells[_columnUnit.Index].Value = g.Unit;
				row.Cells[_columnAmount.Index].Value = 0m;
			}
		}

		/// <summary>
		/// Вызывается при нажатии кнопки ОК.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnOK_Click(object sender, EventArgs e)
		{
			SaveCommand(this, EventArgs.Empty);
			Close();
		}

		/// <summary>
		/// Вызывается при нажатии кнопки Отмена.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnCancel_Click(object sender, EventArgs e)
		{
			Close();
		}

		bool IView.ShowDialog()
		{
			return ShowDialog() == DialogResult.OK;
		}
	}
}

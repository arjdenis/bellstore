﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Arefjev.Bell.Storage.BL;

namespace Arefjev.Bell.UI.WinForms
{
	public sealed class Confirmation : IConfirmation
	{
		public bool ShowQuestion(string question)
		{
			return MessageBox.Show(question, Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes;
		}
	}
}

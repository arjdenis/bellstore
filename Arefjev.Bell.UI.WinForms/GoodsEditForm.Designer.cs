﻿namespace Arefjev.Bell.UI.WinForms
{
	partial class GoodsEditForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this._buttonCancel = new System.Windows.Forms.Button();
			this._buttonOK = new System.Windows.Forms.Button();
			this._textBoxArt = new System.Windows.Forms.TextBox();
			this._textBoxName = new System.Windows.Forms.TextBox();
			this._textBoxUnit = new System.Windows.Forms.TextBox();
			this._numericUpDownPrice = new System.Windows.Forms.NumericUpDown();
			this._errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this._numericUpDownPrice)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this._errorProvider)).BeginInit();
			this.SuspendLayout();
			// 
			// _buttonCancel
			// 
			this._buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this._buttonCancel.CausesValidation = false;
			this._buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this._buttonCancel.Location = new System.Drawing.Point(197, 177);
			this._buttonCancel.Name = "_buttonCancel";
			this._buttonCancel.Size = new System.Drawing.Size(75, 23);
			this._buttonCancel.TabIndex = 5;
			this._buttonCancel.Text = "Cancel";
			this._buttonCancel.UseVisualStyleBackColor = true;
			// 
			// _buttonOK
			// 
			this._buttonOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this._buttonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this._buttonOK.Location = new System.Drawing.Point(116, 177);
			this._buttonOK.Name = "_buttonOK";
			this._buttonOK.Size = new System.Drawing.Size(75, 23);
			this._buttonOK.TabIndex = 4;
			this._buttonOK.Text = "OK";
			this._buttonOK.UseVisualStyleBackColor = true;
			// 
			// _textBoxArt
			// 
			this._textBoxArt.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._errorProvider.SetIconPadding(this._textBoxArt, -24);
			this._textBoxArt.Location = new System.Drawing.Point(78, 12);
			this._textBoxArt.Name = "_textBoxArt";
			this._textBoxArt.Size = new System.Drawing.Size(194, 20);
			this._textBoxArt.TabIndex = 0;
			this._textBoxArt.Validating += new System.ComponentModel.CancelEventHandler(this.OnArt_Validating);
			// 
			// _textBoxName
			// 
			this._textBoxName.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._errorProvider.SetIconPadding(this._textBoxName, -24);
			this._textBoxName.Location = new System.Drawing.Point(78, 38);
			this._textBoxName.Multiline = true;
			this._textBoxName.Name = "_textBoxName";
			this._textBoxName.Size = new System.Drawing.Size(194, 68);
			this._textBoxName.TabIndex = 1;
			this._textBoxName.Validating += new System.ComponentModel.CancelEventHandler(this.OnName_Validating);
			// 
			// _textBoxUnit
			// 
			this._textBoxUnit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._textBoxUnit.Location = new System.Drawing.Point(78, 112);
			this._textBoxUnit.Name = "_textBoxUnit";
			this._textBoxUnit.Size = new System.Drawing.Size(194, 20);
			this._textBoxUnit.TabIndex = 2;
			// 
			// _numericUpDownPrice
			// 
			this._numericUpDownPrice.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._numericUpDownPrice.DecimalPlaces = 2;
			this._numericUpDownPrice.Location = new System.Drawing.Point(78, 138);
			this._numericUpDownPrice.Name = "_numericUpDownPrice";
			this._numericUpDownPrice.Size = new System.Drawing.Size(194, 20);
			this._numericUpDownPrice.TabIndex = 3;
			// 
			// _errorProvider
			// 
			this._errorProvider.ContainerControl = this;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 15);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(39, 13);
			this.label1.TabIndex = 6;
			this.label1.Text = "Article:";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(12, 41);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(38, 13);
			this.label2.TabIndex = 7;
			this.label2.Text = "Name:";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(12, 115);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(29, 13);
			this.label3.TabIndex = 8;
			this.label3.Text = "Unit:";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(12, 140);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(34, 13);
			this.label4.TabIndex = 9;
			this.label4.Text = "Price:";
			// 
			// GoodsEditForm
			// 
			this.AcceptButton = this._buttonOK;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this._buttonCancel;
			this.ClientSize = new System.Drawing.Size(284, 212);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this._numericUpDownPrice);
			this.Controls.Add(this._textBoxName);
			this.Controls.Add(this._textBoxUnit);
			this.Controls.Add(this._textBoxArt);
			this.Controls.Add(this._buttonOK);
			this.Controls.Add(this._buttonCancel);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Name = "GoodsEditForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Goods edit";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.GoodsEditForm_FormClosing);
			this.Load += new System.EventHandler(this.GoodsEditForm_Load);
			((System.ComponentModel.ISupportInitialize)(this._numericUpDownPrice)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this._errorProvider)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button _buttonCancel;
		private System.Windows.Forms.Button _buttonOK;
		private System.Windows.Forms.TextBox _textBoxArt;
		private System.Windows.Forms.TextBox _textBoxName;
		private System.Windows.Forms.TextBox _textBoxUnit;
		private System.Windows.Forms.NumericUpDown _numericUpDownPrice;
		private System.Windows.Forms.ErrorProvider _errorProvider;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
	}
}
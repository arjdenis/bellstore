﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Arefjev.Bell.Storage.BL;
using Arefjev.Bell.Storage.BL.Model;
using Arefjev.Bell.Storage.BL.View;

namespace Arefjev.Bell.UI.WinForms
{
	/// <summary>
	/// Редактор найма продавца.
	/// </summary>
	public partial class HireAssistentForm : Form, IHireAssistentView
	{
		/// <summary>
		/// Инициализирует новый экземпляр.
		/// </summary>
		/// <param name="assistent">Модель продавца.</param>
		public HireAssistentForm(ShopAssistent assistent)
		{
			InitializeComponent();

			Assistent = assistent ?? new ShopAssistent();
		}

		/// <summary>
		/// Возвращает/устанавливает модель продавца.
		/// </summary>
		public ShopAssistent Assistent { get; set; }

		/// <summary>
		/// Отображает редактор в режиме диалога.
		/// </summary>
		/// <returns>true, если редактирование завершено успешно, иначе false.</returns>
		bool IView.ShowDialog()
		{
			return ShowDialog() == DialogResult.OK;
		}

		/// <summary>
		/// Вызывается при отображеии редактора.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void HireAssistentForm_Load(object sender, EventArgs e)
		{
			_textBoxFIO.DataBindings.Add("Text", Assistent, "FullName");
			_textBoxPassport.DataBindings.Add("Text", Assistent, "PassportId");
		}

		/// <summary>
		/// Вызывается при проверке ФИО продавца.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnFIO_Validating(object sender, CancelEventArgs e)
		{
			_errorProvider.SetError(_textBoxFIO, null);

			if (string.IsNullOrEmpty(_textBoxFIO.Text))
			{
				_errorProvider.SetError(_textBoxFIO, "FIO is empty!");
				e.Cancel = true;
			}
		}

		/// <summary>
		/// Вызывается при проверке номера паспорта продавца.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnPassport_Validating(object sender, CancelEventArgs e)
		{
			_errorProvider.SetError(_textBoxPassport, null);

			if (string.IsNullOrEmpty(_textBoxPassport.Text))
			{
				_errorProvider.SetError(_textBoxPassport, "Passport id is empty!");
				e.Cancel = true;
			}
		}

		/// <summary>
		/// Вызывается при закрытии редактора.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void HireAssistentForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (DialogResult == DialogResult.OK)
				if (ValidateChildren() == false)
				{
					e.Cancel = true;
				}
		}
	}
}

﻿namespace Arefjev.Bell.UI.WinForms
{
	partial class HireAssistentForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this._buttonOK = new System.Windows.Forms.Button();
			this._buttonCancel = new System.Windows.Forms.Button();
			this._labelFIO = new System.Windows.Forms.Label();
			this._labelPassportId = new System.Windows.Forms.Label();
			this._textBoxFIO = new System.Windows.Forms.TextBox();
			this._textBoxPassport = new System.Windows.Forms.TextBox();
			this._errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
			((System.ComponentModel.ISupportInitialize)(this._errorProvider)).BeginInit();
			this.SuspendLayout();
			// 
			// _buttonOK
			// 
			this._buttonOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this._buttonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this._buttonOK.Location = new System.Drawing.Point(227, 70);
			this._buttonOK.Name = "_buttonOK";
			this._buttonOK.Size = new System.Drawing.Size(75, 23);
			this._buttonOK.TabIndex = 4;
			this._buttonOK.Text = "OK";
			this._buttonOK.UseVisualStyleBackColor = true;
			// 
			// _buttonCancel
			// 
			this._buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this._buttonCancel.CausesValidation = false;
			this._buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this._buttonCancel.Location = new System.Drawing.Point(308, 70);
			this._buttonCancel.Name = "_buttonCancel";
			this._buttonCancel.Size = new System.Drawing.Size(75, 23);
			this._buttonCancel.TabIndex = 5;
			this._buttonCancel.Text = "Cancel";
			this._buttonCancel.UseVisualStyleBackColor = true;
			// 
			// _labelFIO
			// 
			this._labelFIO.AutoSize = true;
			this._labelFIO.Location = new System.Drawing.Point(12, 15);
			this._labelFIO.Name = "_labelFIO";
			this._labelFIO.Size = new System.Drawing.Size(24, 13);
			this._labelFIO.TabIndex = 0;
			this._labelFIO.Text = "FIO";
			// 
			// _labelPassportId
			// 
			this._labelPassportId.AutoSize = true;
			this._labelPassportId.Location = new System.Drawing.Point(12, 41);
			this._labelPassportId.Name = "_labelPassportId";
			this._labelPassportId.Size = new System.Drawing.Size(62, 13);
			this._labelPassportId.TabIndex = 2;
			this._labelPassportId.Text = "Passport id:";
			// 
			// _textBoxFIO
			// 
			this._textBoxFIO.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._errorProvider.SetIconPadding(this._textBoxFIO, -24);
			this._textBoxFIO.Location = new System.Drawing.Point(105, 12);
			this._textBoxFIO.Name = "_textBoxFIO";
			this._textBoxFIO.Size = new System.Drawing.Size(278, 20);
			this._textBoxFIO.TabIndex = 1;
			this._textBoxFIO.Validating += new System.ComponentModel.CancelEventHandler(this.OnFIO_Validating);
			// 
			// _textBoxPassport
			// 
			this._textBoxPassport.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._errorProvider.SetIconPadding(this._textBoxPassport, -24);
			this._textBoxPassport.Location = new System.Drawing.Point(105, 38);
			this._textBoxPassport.Name = "_textBoxPassport";
			this._textBoxPassport.Size = new System.Drawing.Size(278, 20);
			this._textBoxPassport.TabIndex = 3;
			this._textBoxPassport.Validating += new System.ComponentModel.CancelEventHandler(this.OnPassport_Validating);
			// 
			// _errorProvider
			// 
			this._errorProvider.ContainerControl = this;
			// 
			// HireAssistentForm
			// 
			this.AcceptButton = this._buttonOK;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this._buttonCancel;
			this.ClientSize = new System.Drawing.Size(395, 105);
			this.Controls.Add(this._textBoxPassport);
			this.Controls.Add(this._textBoxFIO);
			this.Controls.Add(this._labelPassportId);
			this.Controls.Add(this._labelFIO);
			this.Controls.Add(this._buttonCancel);
			this.Controls.Add(this._buttonOK);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Name = "HireAssistentForm";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "HireAssistentForm";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.HireAssistentForm_FormClosing);
			this.Load += new System.EventHandler(this.HireAssistentForm_Load);
			((System.ComponentModel.ISupportInitialize)(this._errorProvider)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button _buttonOK;
		private System.Windows.Forms.Button _buttonCancel;
		private System.Windows.Forms.Label _labelFIO;
		private System.Windows.Forms.Label _labelPassportId;
		private System.Windows.Forms.TextBox _textBoxFIO;
		private System.Windows.Forms.TextBox _textBoxPassport;
		private System.Windows.Forms.ErrorProvider _errorProvider;
	}
}
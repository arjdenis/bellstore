﻿namespace Arefjev.Bell.UI.WinForms
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this._menu = new System.Windows.Forms.MenuStrip();
			this._storeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this._goodsInStoreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this._goodsIncomingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this._toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this._exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this._catalogsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this._goodsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this._assistentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this._toolStripMenuItemSell = new System.Windows.Forms.ToolStripMenuItem();
			this._toolStripMenuItemChecks = new System.Windows.Forms.ToolStripMenuItem();
			this._menu.SuspendLayout();
			this.SuspendLayout();
			// 
			// _menu
			// 
			this._menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._storeToolStripMenuItem,
            this._catalogsToolStripMenuItem,
            this._toolStripMenuItemSell});
			this._menu.Location = new System.Drawing.Point(0, 0);
			this._menu.Name = "_menu";
			this._menu.Size = new System.Drawing.Size(784, 24);
			this._menu.TabIndex = 1;
			// 
			// _storeToolStripMenuItem
			// 
			this._storeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._goodsInStoreToolStripMenuItem,
            this._goodsIncomingToolStripMenuItem,
            this._toolStripSeparator1,
            this._exitToolStripMenuItem});
			this._storeToolStripMenuItem.Name = "_storeToolStripMenuItem";
			this._storeToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
			this._storeToolStripMenuItem.Text = "Store";
			// 
			// _goodsInStoreToolStripMenuItem
			// 
			this._goodsInStoreToolStripMenuItem.Name = "_goodsInStoreToolStripMenuItem";
			this._goodsInStoreToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
			this._goodsInStoreToolStripMenuItem.Text = "Goods in store";
			this._goodsInStoreToolStripMenuItem.Click += new System.EventHandler(this.OnGoodsInStore_Click);
			// 
			// _goodsIncomingToolStripMenuItem
			// 
			this._goodsIncomingToolStripMenuItem.Name = "_goodsIncomingToolStripMenuItem";
			this._goodsIncomingToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
			this._goodsIncomingToolStripMenuItem.Text = "Goods incoming";
			this._goodsIncomingToolStripMenuItem.Click += new System.EventHandler(this.OnGoodsIncoming_Click);
			// 
			// _toolStripSeparator1
			// 
			this._toolStripSeparator1.Name = "_toolStripSeparator1";
			this._toolStripSeparator1.Size = new System.Drawing.Size(159, 6);
			// 
			// _exitToolStripMenuItem
			// 
			this._exitToolStripMenuItem.Name = "_exitToolStripMenuItem";
			this._exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
			this._exitToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
			this._exitToolStripMenuItem.Text = "Exit";
			this._exitToolStripMenuItem.Click += new System.EventHandler(this.OnExit_Click);
			// 
			// _catalogsToolStripMenuItem
			// 
			this._catalogsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._goodsToolStripMenuItem,
            this._assistentsToolStripMenuItem});
			this._catalogsToolStripMenuItem.Name = "_catalogsToolStripMenuItem";
			this._catalogsToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
			this._catalogsToolStripMenuItem.Text = "Catalogs";
			// 
			// _goodsToolStripMenuItem
			// 
			this._goodsToolStripMenuItem.Name = "_goodsToolStripMenuItem";
			this._goodsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this._goodsToolStripMenuItem.Text = "Goods";
			this._goodsToolStripMenuItem.Click += new System.EventHandler(this.OnGoods_Click);
			// 
			// _assistentsToolStripMenuItem
			// 
			this._assistentsToolStripMenuItem.Name = "_assistentsToolStripMenuItem";
			this._assistentsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this._assistentsToolStripMenuItem.Text = "Assistents";
			this._assistentsToolStripMenuItem.Click += new System.EventHandler(this.OnAssistents_Click);
			// 
			// _toolStripMenuItemSell
			// 
			this._toolStripMenuItemSell.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._toolStripMenuItemChecks});
			this._toolStripMenuItemSell.Name = "_toolStripMenuItemSell";
			this._toolStripMenuItemSell.Size = new System.Drawing.Size(37, 20);
			this._toolStripMenuItemSell.Text = "Sell";
			// 
			// _toolStripMenuItemChecks
			// 
			this._toolStripMenuItemChecks.Name = "_toolStripMenuItemChecks";
			this._toolStripMenuItemChecks.Size = new System.Drawing.Size(152, 22);
			this._toolStripMenuItemChecks.Text = "Checks";
			this._toolStripMenuItemChecks.Click += new System.EventHandler(this.OnChecks_Click);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(784, 561);
			this.Controls.Add(this._menu);
			this.IsMdiContainer = true;
			this.MainMenuStrip = this._menu;
			this.Name = "MainForm";
			this.ShowIcon = false;
			this.Text = "MainForm";
			this._menu.ResumeLayout(false);
			this._menu.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.MenuStrip _menu;
		private System.Windows.Forms.ToolStripMenuItem _storeToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem _goodsInStoreToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator _toolStripSeparator1;
		private System.Windows.Forms.ToolStripMenuItem _exitToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem _catalogsToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem _goodsToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem _assistentsToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem _goodsIncomingToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem _toolStripMenuItemSell;
		private System.Windows.Forms.ToolStripMenuItem _toolStripMenuItemChecks;
	}
}
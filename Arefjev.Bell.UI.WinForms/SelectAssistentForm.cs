﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Arefjev.Bell.Storage.BL;
using Arefjev.Bell.Storage.BL.Model;
using Arefjev.Bell.Storage.BL.View;

namespace Arefjev.Bell.UI.WinForms
{
	public partial class SelectAssistentForm : Form, ISelectAssistentView
	{
		public SelectAssistentForm()
		{
			InitializeComponent();
		}

		public ShopAssistent Assistent
		{
			get
			{
				return _listBox.SelectedItem as ShopAssistent;
			}
			set
			{
				_listBox.SelectedValue = value == null ? -1 : value.Id;
			}
		}

		public void LoadAssistents(List<ShopAssistent> list)
		{
			_bindingSource.DataSource = list;
		}

		bool IView.ShowDialog()
		{
			return ShowDialog() == DialogResult.OK;
		}
	}
}

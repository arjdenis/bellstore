﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Arefjev.Bell.Storage.BL;
using Arefjev.Bell.Storage.BL.Model;
using Arefjev.Bell.Storage.BL.View;

namespace Arefjev.Bell.UI.WinForms
{
	/// <summary>
	/// Форма редактирования списка товаров.
	/// </summary>
	public partial class GoodsForm : Form, IGoodsView
	{
		public GoodsForm()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Вызывается при нажатии кнопки Добавить товар.
		/// Открывает форму добавления товара.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Парметры события.</param>
		private void OnAdd_Click(object sender, EventArgs e)
		{
			AddGoodsCommand(this, EventArgs.Empty);
		}

		/// <summary>
		/// Вызывается при нажатии кнопки Редактировать.
		/// Открывает окно редактирования товара.
		/// В случае успешного редактирования изменения отправляются в БД.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Парметры события.</param>
		private void OnButtonEdit_Click(object sender, EventArgs e)
		{
			EditGoodsCommand(this, EventArgs.Empty);
		}

		/// <summary>
		/// Вызывается при нажатии кнопки Удалить товар.
		/// Удаляет товар в БД.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Парметры события.</param>
		private void OnDelete_Click(object sender, EventArgs e)
		{
			DeleteGoodsCommand(this, EventArgs.Empty);
		}

		/// <summary>
		/// Вызывается при завершении редактирования строки таблицы.
		/// Сбрасывается текст ошибки.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Парметры события.</param>
		private void OnDataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
		{
			_dataGridView.Rows[e.RowIndex].ErrorText = null;
		}

		/// <summary>
		/// Вызывается после изменения значения ячейки.
		/// Отправляет изменения на сервер.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Парметры события.</param>
		private void OnDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
		{
		}

		/// <summary>
		/// Вызывается при нажатии кнопки Сохранить.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Парметры события.</param>
		private void OnSave_Click(object sender, EventArgs e)
		{
			SaveCommand(this, EventArgs.Empty);
			Close();
		}

		/// <summary>
		/// Вызывается при нажатии кнопки Закрыть.
		/// Закрывает эту форму.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Парметры события.</param>
		private void OnButtonClose_Click(object sender, EventArgs e)
		{
			Close();
		}

		/// <summary>
		/// Вызывается перед изменением значения ячейки.
		/// Проверяет корректно ли задан артикул и наименование товара.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Парметры события.</param>
		private void OnDataGridView_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (e.RowIndex < 0 || e.RowIndex >= _bindingSource.Count)
				return;

			if (e.ColumnIndex == _articleDataGridViewTextBoxColumn.Index)
			{
				_dataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = null;
                if (string.IsNullOrEmpty((string)e.FormattedValue))
				{
					e.Cancel = true;
					_dataGridView.Rows[e.RowIndex].ErrorText = "Article is not set!";
				}
			}
			else if (e.ColumnIndex == _nameDataGridViewTextBoxColumn.Index)
			{
				_dataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = null;
				if (string.IsNullOrEmpty((string)e.FormattedValue))
				{
					e.Cancel = true;
					_dataGridView.Rows[e.RowIndex].ErrorText = "Name is not set!";
				}
			}
		}

		/// <summary>
		/// Загружает список товаров.
		/// </summary>
		/// <param name="goods">Список товаров.</param>
		public void LoadGoods(IList<Goods> goods)
		{
			_bindingSource.DataSource = goods;
		}

		bool IView.ShowDialog()
		{
			return ShowDialog() == DialogResult.OK;
		}

		/// <summary>
		/// Возвращает выбранные товары.
		/// </summary>
		public IEnumerable<Goods> SelectedGoods
		{
			get
			{
				return _dataGridView.SelectedRows
					.OfType<DataGridViewRow>()
					.Select(row => (Goods)row.DataBoundItem);
			}
		}

		/// <summary>
		/// Событие добавления товара.
		/// </summary>
		public event EventHandler AddGoodsCommand = delegate { };

		/// <summary>
		/// Событие редактирования товара.
		/// </summary>
		public event EventHandler EditGoodsCommand = delegate { };

		/// <summary>
		/// Событие удаления товара.
		/// </summary>
		public event EventHandler DeleteGoodsCommand = delegate { };

		/// <summary>
		/// Команда на схранение данных.
		/// </summary>
		public event EventHandler SaveCommand = delegate { };
	}
}

﻿namespace Arefjev.Bell.UI.WinForms
{
	partial class GoodsForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				//_context.Dispose();
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this._dataGridView = new System.Windows.Forms.DataGridView();
			this._idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this._articleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this._nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this._unitDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this._priceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this._amountOfGoodsColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this._bindingSource = new System.Windows.Forms.BindingSource(this.components);
			this._buttonClose = new System.Windows.Forms.Button();
			this._buttonAdd = new System.Windows.Forms.Button();
			this._buttonEdit = new System.Windows.Forms.Button();
			this._buttonDelete = new System.Windows.Forms.Button();
			this._buttonSave = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this._dataGridView)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this._bindingSource)).BeginInit();
			this.SuspendLayout();
			// 
			// _dataGridView
			// 
			this._dataGridView.AllowUserToAddRows = false;
			this._dataGridView.AllowUserToDeleteRows = false;
			this._dataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._dataGridView.AutoGenerateColumns = false;
			this._dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this._dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._idDataGridViewTextBoxColumn,
            this._articleDataGridViewTextBoxColumn,
            this._nameDataGridViewTextBoxColumn,
            this._unitDataGridViewTextBoxColumn,
            this._priceDataGridViewTextBoxColumn,
            this._amountOfGoodsColumn});
			this._dataGridView.DataSource = this._bindingSource;
			this._dataGridView.Location = new System.Drawing.Point(12, 12);
			this._dataGridView.Name = "_dataGridView";
			this._dataGridView.Size = new System.Drawing.Size(583, 300);
			this._dataGridView.TabIndex = 0;
			this._dataGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.OnDataGridView_CellEndEdit);
			this._dataGridView.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.OnDataGridView_CellValidating);
			this._dataGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.OnDataGridView_CellValueChanged);
			// 
			// _idDataGridViewTextBoxColumn
			// 
			this._idDataGridViewTextBoxColumn.DataPropertyName = "Id";
			this._idDataGridViewTextBoxColumn.HeaderText = "Id";
			this._idDataGridViewTextBoxColumn.Name = "_idDataGridViewTextBoxColumn";
			this._idDataGridViewTextBoxColumn.Visible = false;
			// 
			// _articleDataGridViewTextBoxColumn
			// 
			this._articleDataGridViewTextBoxColumn.DataPropertyName = "Article";
			this._articleDataGridViewTextBoxColumn.HeaderText = "Article";
			this._articleDataGridViewTextBoxColumn.MinimumWidth = 50;
			this._articleDataGridViewTextBoxColumn.Name = "_articleDataGridViewTextBoxColumn";
			// 
			// _nameDataGridViewTextBoxColumn
			// 
			this._nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
			this._nameDataGridViewTextBoxColumn.HeaderText = "Name";
			this._nameDataGridViewTextBoxColumn.MinimumWidth = 50;
			this._nameDataGridViewTextBoxColumn.Name = "_nameDataGridViewTextBoxColumn";
			this._nameDataGridViewTextBoxColumn.Width = 200;
			// 
			// _unitDataGridViewTextBoxColumn
			// 
			this._unitDataGridViewTextBoxColumn.DataPropertyName = "Unit";
			this._unitDataGridViewTextBoxColumn.HeaderText = "Unit";
			this._unitDataGridViewTextBoxColumn.MinimumWidth = 50;
			this._unitDataGridViewTextBoxColumn.Name = "_unitDataGridViewTextBoxColumn";
			// 
			// _priceDataGridViewTextBoxColumn
			// 
			this._priceDataGridViewTextBoxColumn.DataPropertyName = "Price";
			this._priceDataGridViewTextBoxColumn.HeaderText = "Price";
			this._priceDataGridViewTextBoxColumn.MinimumWidth = 50;
			this._priceDataGridViewTextBoxColumn.Name = "_priceDataGridViewTextBoxColumn";
			// 
			// _amountOfGoodsColumn
			// 
			this._amountOfGoodsColumn.DataPropertyName = "AmountOfGoods";
			this._amountOfGoodsColumn.HeaderText = "AmountOfGoods";
			this._amountOfGoodsColumn.MinimumWidth = 50;
			this._amountOfGoodsColumn.Name = "_amountOfGoodsColumn";
			this._amountOfGoodsColumn.ReadOnly = true;
			this._amountOfGoodsColumn.Visible = false;
			// 
			// _bindingSource
			// 
			this._bindingSource.AllowNew = false;
			this._bindingSource.DataSource = typeof(Arefjev.Bell.Storage.BL.Model.Goods);
			// 
			// _buttonClose
			// 
			this._buttonClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this._buttonClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this._buttonClose.Location = new System.Drawing.Point(520, 318);
			this._buttonClose.Name = "_buttonClose";
			this._buttonClose.Size = new System.Drawing.Size(75, 23);
			this._buttonClose.TabIndex = 0;
			this._buttonClose.Text = "Close";
			this._buttonClose.UseVisualStyleBackColor = true;
			this._buttonClose.Click += new System.EventHandler(this.OnButtonClose_Click);
			// 
			// _buttonAdd
			// 
			this._buttonAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this._buttonAdd.Location = new System.Drawing.Point(12, 318);
			this._buttonAdd.Name = "_buttonAdd";
			this._buttonAdd.Size = new System.Drawing.Size(75, 23);
			this._buttonAdd.TabIndex = 1;
			this._buttonAdd.Text = "Add";
			this._buttonAdd.UseVisualStyleBackColor = true;
			this._buttonAdd.Click += new System.EventHandler(this.OnAdd_Click);
			// 
			// _buttonEdit
			// 
			this._buttonEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this._buttonEdit.Location = new System.Drawing.Point(93, 318);
			this._buttonEdit.Name = "_buttonEdit";
			this._buttonEdit.Size = new System.Drawing.Size(75, 23);
			this._buttonEdit.TabIndex = 2;
			this._buttonEdit.Text = "Edit";
			this._buttonEdit.UseVisualStyleBackColor = true;
			this._buttonEdit.Click += new System.EventHandler(this.OnButtonEdit_Click);
			// 
			// _buttonDelete
			// 
			this._buttonDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this._buttonDelete.Location = new System.Drawing.Point(174, 318);
			this._buttonDelete.Name = "_buttonDelete";
			this._buttonDelete.Size = new System.Drawing.Size(75, 23);
			this._buttonDelete.TabIndex = 3;
			this._buttonDelete.Text = "Delete";
			this._buttonDelete.UseVisualStyleBackColor = true;
			this._buttonDelete.Click += new System.EventHandler(this.OnDelete_Click);
			// 
			// _buttonSave
			// 
			this._buttonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this._buttonSave.DialogResult = System.Windows.Forms.DialogResult.OK;
			this._buttonSave.Location = new System.Drawing.Point(439, 318);
			this._buttonSave.Name = "_buttonSave";
			this._buttonSave.Size = new System.Drawing.Size(75, 23);
			this._buttonSave.TabIndex = 4;
			this._buttonSave.Text = "Save";
			this._buttonSave.UseVisualStyleBackColor = true;
			this._buttonSave.Click += new System.EventHandler(this.OnSave_Click);
			// 
			// GoodsForm
			// 
			this.AcceptButton = this._buttonSave;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this._buttonClose;
			this.ClientSize = new System.Drawing.Size(607, 353);
			this.Controls.Add(this._buttonSave);
			this.Controls.Add(this._buttonDelete);
			this.Controls.Add(this._buttonEdit);
			this.Controls.Add(this._buttonAdd);
			this.Controls.Add(this._buttonClose);
			this.Controls.Add(this._dataGridView);
			this.Name = "GoodsForm";
			this.ShowIcon = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Goods";
			((System.ComponentModel.ISupportInitialize)(this._dataGridView)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this._bindingSource)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.DataGridView _dataGridView;
		private System.Windows.Forms.BindingSource _bindingSource;
		private System.Windows.Forms.Button _buttonClose;
		private System.Windows.Forms.Button _buttonAdd;
		private System.Windows.Forms.Button _buttonEdit;
		private System.Windows.Forms.Button _buttonDelete;
		private System.Windows.Forms.DataGridViewTextBoxColumn _idDataGridViewTextBoxColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn _articleDataGridViewTextBoxColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn _nameDataGridViewTextBoxColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn _unitDataGridViewTextBoxColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn _priceDataGridViewTextBoxColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn _amountOfGoodsColumn;
		private System.Windows.Forms.Button _buttonSave;
	}
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Arefjev.Bell.Storage.BL;
using Arefjev.Bell.Storage.BL.Model;
using Arefjev.Bell.Storage.BL.View;

namespace Arefjev.Bell.UI.WinForms
{
	public partial class GoodsInStoreForm : Form, IGoodsInStoreView
	{
		public GoodsInStoreForm()
		{
			InitializeComponent();
		}

		public IList<Goods> SelectedGoods
		{
			get
			{
				return _dataGridView.SelectedRows
					.OfType<DataGridViewRow>()
					.Select(row => (Goods)row.DataBoundItem)
					.ToList();
			}
		}

		public void LoadGoods(IList<Goods> goods)
		{
			_bindingSource.DataSource = goods;
		}

		private void OnClose_Click(object sender, EventArgs e)
		{
			Close();
		}

		bool IView.ShowDialog()
		{
			return ShowDialog() == DialogResult.OK;
		}
	}
}

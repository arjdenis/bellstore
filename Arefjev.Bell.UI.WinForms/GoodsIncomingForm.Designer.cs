﻿namespace Arefjev.Bell.UI.WinForms
{
	partial class GoodsIncomingForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
			this._dataGridView = new System.Windows.Forms.DataGridView();
			this._columnArticle = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this._columnName = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this._columnUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this._columnAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this._buttonOK = new System.Windows.Forms.Button();
			this._buttonCancel = new System.Windows.Forms.Button();
			this._buttonAdd = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this._dataGridView)).BeginInit();
			this.SuspendLayout();
			// 
			// _dataGridView
			// 
			this._dataGridView.AllowUserToAddRows = false;
			this._dataGridView.AllowUserToDeleteRows = false;
			this._dataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this._dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._columnArticle,
            this._columnName,
            this._columnUnit,
            this._columnAmount});
			this._dataGridView.Location = new System.Drawing.Point(12, 12);
			this._dataGridView.Name = "_dataGridView";
			this._dataGridView.Size = new System.Drawing.Size(590, 358);
			this._dataGridView.TabIndex = 0;
			// 
			// _columnArticle
			// 
			this._columnArticle.HeaderText = "Article";
			this._columnArticle.MinimumWidth = 50;
			this._columnArticle.Name = "_columnArticle";
			this._columnArticle.ReadOnly = true;
			// 
			// _columnName
			// 
			this._columnName.HeaderText = "Name";
			this._columnName.MinimumWidth = 50;
			this._columnName.Name = "_columnName";
			this._columnName.ReadOnly = true;
			this._columnName.Width = 200;
			// 
			// _columnUnit
			// 
			this._columnUnit.HeaderText = "Unit";
			this._columnUnit.MinimumWidth = 50;
			this._columnUnit.Name = "_columnUnit";
			this._columnUnit.ReadOnly = true;
			// 
			// _columnAmount
			// 
			dataGridViewCellStyle2.Format = "N3";
			dataGridViewCellStyle2.NullValue = null;
			this._columnAmount.DefaultCellStyle = dataGridViewCellStyle2;
			this._columnAmount.HeaderText = "Amount";
			this._columnAmount.MinimumWidth = 50;
			this._columnAmount.Name = "_columnAmount";
			// 
			// _buttonOK
			// 
			this._buttonOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this._buttonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this._buttonOK.Location = new System.Drawing.Point(446, 376);
			this._buttonOK.Name = "_buttonOK";
			this._buttonOK.Size = new System.Drawing.Size(75, 23);
			this._buttonOK.TabIndex = 1;
			this._buttonOK.Text = "OK";
			this._buttonOK.UseVisualStyleBackColor = true;
			this._buttonOK.Click += new System.EventHandler(this.OnOK_Click);
			// 
			// _buttonCancel
			// 
			this._buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this._buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this._buttonCancel.Location = new System.Drawing.Point(527, 376);
			this._buttonCancel.Name = "_buttonCancel";
			this._buttonCancel.Size = new System.Drawing.Size(75, 23);
			this._buttonCancel.TabIndex = 2;
			this._buttonCancel.Text = "Cancel";
			this._buttonCancel.UseVisualStyleBackColor = true;
			this._buttonCancel.Click += new System.EventHandler(this.OnCancel_Click);
			// 
			// _buttonAdd
			// 
			this._buttonAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this._buttonAdd.Location = new System.Drawing.Point(12, 376);
			this._buttonAdd.Name = "_buttonAdd";
			this._buttonAdd.Size = new System.Drawing.Size(75, 23);
			this._buttonAdd.TabIndex = 3;
			this._buttonAdd.Text = "Add";
			this._buttonAdd.UseVisualStyleBackColor = true;
			// 
			// GoodsIncomingForm
			// 
			this.AcceptButton = this._buttonOK;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this._buttonCancel;
			this.ClientSize = new System.Drawing.Size(614, 411);
			this.Controls.Add(this._buttonAdd);
			this.Controls.Add(this._buttonCancel);
			this.Controls.Add(this._buttonOK);
			this.Controls.Add(this._dataGridView);
			this.Name = "GoodsIncomingForm";
			this.ShowIcon = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Goods incoming";
			((System.ComponentModel.ISupportInitialize)(this._dataGridView)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.DataGridView _dataGridView;
		private System.Windows.Forms.Button _buttonOK;
		private System.Windows.Forms.Button _buttonCancel;
		private System.Windows.Forms.DataGridViewTextBoxColumn _columnArticle;
		private System.Windows.Forms.DataGridViewTextBoxColumn _columnName;
		private System.Windows.Forms.DataGridViewTextBoxColumn _columnUnit;
		private System.Windows.Forms.DataGridViewTextBoxColumn _columnAmount;
		private System.Windows.Forms.Button _buttonAdd;
	}
}
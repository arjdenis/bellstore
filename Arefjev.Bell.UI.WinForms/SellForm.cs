﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Arefjev.Bell.Storage.BL;
using Arefjev.Bell.Storage.BL.View;

namespace Arefjev.Bell.UI.WinForms
{
	/// <summary>
	/// Редактор продажи по чеку.
	/// </summary>
	public partial class SellForm : Form, ISellView
	{
		/// <summary>
		/// Инициализирует новый экземпляр.
		/// </summary>
		public SellForm()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Возвращает список выбранных элементов чекаю
		/// </summary>
		public IList<SellItemViewModel> SelectedItems
		{
			get
			{
				return _dataGridView.SelectedRows
					.OfType<DataGridViewRow>()
					.Select(row => (SellItemViewModel)row.DataBoundItem)
					.ToList();
			}
		}

		public CheckViewModel Sell { get; set; }

		public IList<SellItemViewModel> Items
		{
			get
			{
				return _bindingSource.DataSource as IList<SellItemViewModel>;
			}
			set
			{
				_bindingSource.DataSource = value;
			}
		}

		/// <summary>
		/// Команда на добавление элемента чека.
		/// </summary>
		public event EventHandler AddGoodsCommand = delegate { };

		/// <summary>
		/// Команда на печать чека.
		/// </summary>
		public event EventHandler PrintCheckCommand = delegate { };

		/// <summary>
		/// Команда на удаление элемента чека.
		/// </summary>
		public event EventHandler RemoveGoodsCommand = delegate { };

		/// <summary>
		/// Команда на сохранение чека.
		/// </summary>
		public event EventHandler SaveCommand = delegate { };

		/// <summary>
		/// Команда выбора продавца.
		/// </summary>
		public event EventHandler SelectAssistentCommand = delegate { };

		/// <summary>
		/// Команда выбора товара.
		/// </summary>
		public event EventHandler SelectGoodsCommand = delegate { };

		/// <summary>
		/// Команда редактировать отчет.
		/// </summary>
		public event EventHandler DesignCommand = delegate { };

		/// <summary>
		/// Отображает редактор в диалоговом окне.
		/// </summary>
		/// <returns>true, если редактирование завершено успешно, иначе false.</returns>
		bool IView.ShowDialog()
		{
			return ShowDialog() == DialogResult.OK;
		}

		/// <summary>
		/// Вызывается при нажатии кнопки Выбора продавца.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnChangeAssistent_Click(object sender, EventArgs e)
		{
			SelectAssistentCommand(this, EventArgs.Empty);
		}

		/// <summary>
		/// Вызывается при нажатии кнопки Добавить товар.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnAddGoods_Click(object sender, EventArgs e)
		{
			AddGoodsCommand(this, EventArgs.Empty);
        }

		/// <summary>
		/// Вызывается при нажатии кнопки Удалить товар из продаваемых.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnDelete_Click(object sender, EventArgs e)
		{
			RemoveGoodsCommand(this, EventArgs.Empty);
		}

		/// <summary>
		/// Вызывается при нажатии кнопки Печатать чек.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnPrint_Click(object sender, EventArgs e)
		{
			PrintCheckCommand(this, EventArgs.Empty);
		}

		/// <summary>
		/// Вызывается при нажатии кнопки Сохранить.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnSave_Click(object sender, EventArgs e)
		{
			SaveCommand(this, EventArgs.Empty);
		}

		/// <summary>
		/// Вызывается при нажатии кнопки Закрыть редактор.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnCancel_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void OnCellContentClick(object sender, DataGridViewCellEventArgs e)
		{
			if (e.ColumnIndex == _columnChangeGoods.Index)
			{
				SelectGoodsCommand(this, EventArgs.Empty);
			}
		}

		private void SellForm_Load(object sender, EventArgs e)
		{
			_textBoxAssistent.DataBindings.Add("Text", Sell, "AssistentFullName");
			_textBoxSum.DataBindings.Add("Text", Sell, "Sum");
		}

		private void OnDesign_Click(object sender, EventArgs e)
		{
			DesignCommand(this, EventArgs.Empty);
		}
	}
}

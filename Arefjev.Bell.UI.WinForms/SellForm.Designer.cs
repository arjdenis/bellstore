﻿namespace Arefjev.Bell.UI.WinForms
{
	partial class SellForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
			this._labelAssistent = new System.Windows.Forms.Label();
			this._textBoxAssistent = new System.Windows.Forms.TextBox();
			this._buttonChangeAssistent = new System.Windows.Forms.Button();
			this._dataGridView = new System.Windows.Forms.DataGridView();
			this._columnArt = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this._columnName = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this._columnChangeGoods = new System.Windows.Forms.DataGridViewButtonColumn();
			this._columnAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this._columnSum = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this._bindingSource = new System.Windows.Forms.BindingSource(this.components);
			this._buttonSave = new System.Windows.Forms.Button();
			this._buttonCancel = new System.Windows.Forms.Button();
			this._buttonAddGoods = new System.Windows.Forms.Button();
			this._buttonDelete = new System.Windows.Forms.Button();
			this._textBoxSum = new System.Windows.Forms.TextBox();
			this._labelSum = new System.Windows.Forms.Label();
			this._buttonPrint = new System.Windows.Forms.Button();
			this._contextMenuPrint = new System.Windows.Forms.ContextMenuStrip(this.components);
			this._toolStripMenuItemDesign = new System.Windows.Forms.ToolStripMenuItem();
			((System.ComponentModel.ISupportInitialize)(this._dataGridView)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this._bindingSource)).BeginInit();
			this._contextMenuPrint.SuspendLayout();
			this.SuspendLayout();
			// 
			// _labelAssistent
			// 
			this._labelAssistent.AutoSize = true;
			this._labelAssistent.Location = new System.Drawing.Point(12, 15);
			this._labelAssistent.Name = "_labelAssistent";
			this._labelAssistent.Size = new System.Drawing.Size(52, 13);
			this._labelAssistent.TabIndex = 0;
			this._labelAssistent.Text = "Assistent:";
			// 
			// _textBoxAssistent
			// 
			this._textBoxAssistent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._textBoxAssistent.Location = new System.Drawing.Point(70, 12);
			this._textBoxAssistent.Name = "_textBoxAssistent";
			this._textBoxAssistent.ReadOnly = true;
			this._textBoxAssistent.Size = new System.Drawing.Size(521, 20);
			this._textBoxAssistent.TabIndex = 1;
			this._textBoxAssistent.TabStop = false;
			// 
			// _buttonChangeAssistent
			// 
			this._buttonChangeAssistent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this._buttonChangeAssistent.Location = new System.Drawing.Point(597, 10);
			this._buttonChangeAssistent.Name = "_buttonChangeAssistent";
			this._buttonChangeAssistent.Size = new System.Drawing.Size(75, 23);
			this._buttonChangeAssistent.TabIndex = 2;
			this._buttonChangeAssistent.Text = "...";
			this._buttonChangeAssistent.UseVisualStyleBackColor = true;
			this._buttonChangeAssistent.Click += new System.EventHandler(this.OnChangeAssistent_Click);
			// 
			// _dataGridView
			// 
			this._dataGridView.AllowUserToAddRows = false;
			this._dataGridView.AllowUserToDeleteRows = false;
			this._dataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._dataGridView.AutoGenerateColumns = false;
			this._dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this._dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._columnArt,
            this._columnName,
            this._columnChangeGoods,
            this._columnAmount,
            this._columnSum});
			this._dataGridView.DataSource = this._bindingSource;
			this._dataGridView.Location = new System.Drawing.Point(12, 38);
			this._dataGridView.Name = "_dataGridView";
			this._dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this._dataGridView.Size = new System.Drawing.Size(660, 392);
			this._dataGridView.TabIndex = 3;
			this._dataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.OnCellContentClick);
			// 
			// _columnArt
			// 
			this._columnArt.DataPropertyName = "Article";
			this._columnArt.HeaderText = "Article";
			this._columnArt.MinimumWidth = 50;
			this._columnArt.Name = "_columnArt";
			this._columnArt.ReadOnly = true;
			// 
			// _columnName
			// 
			this._columnName.DataPropertyName = "Name";
			this._columnName.HeaderText = "Name";
			this._columnName.MinimumWidth = 50;
			this._columnName.Name = "_columnName";
			this._columnName.ReadOnly = true;
			this._columnName.Width = 200;
			// 
			// _columnChangeGoods
			// 
			this._columnChangeGoods.HeaderText = "";
			this._columnChangeGoods.MinimumWidth = 30;
			this._columnChangeGoods.Name = "_columnChangeGoods";
			this._columnChangeGoods.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this._columnChangeGoods.Text = "...";
			this._columnChangeGoods.Width = 30;
			// 
			// _columnAmount
			// 
			this._columnAmount.DataPropertyName = "Amount";
			dataGridViewCellStyle1.Format = "N2";
			dataGridViewCellStyle1.NullValue = null;
			this._columnAmount.DefaultCellStyle = dataGridViewCellStyle1;
			this._columnAmount.HeaderText = "Amount";
			this._columnAmount.MinimumWidth = 50;
			this._columnAmount.Name = "_columnAmount";
			// 
			// _columnSum
			// 
			this._columnSum.DataPropertyName = "ItemSum";
			dataGridViewCellStyle2.Format = "N2";
			dataGridViewCellStyle2.NullValue = null;
			this._columnSum.DefaultCellStyle = dataGridViewCellStyle2;
			this._columnSum.HeaderText = "Sum";
			this._columnSum.MinimumWidth = 55;
			this._columnSum.Name = "_columnSum";
			this._columnSum.ReadOnly = true;
			this._columnSum.Width = 120;
			// 
			// _buttonSave
			// 
			this._buttonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this._buttonSave.DialogResult = System.Windows.Forms.DialogResult.OK;
			this._buttonSave.Location = new System.Drawing.Point(516, 462);
			this._buttonSave.Name = "_buttonSave";
			this._buttonSave.Size = new System.Drawing.Size(75, 23);
			this._buttonSave.TabIndex = 9;
			this._buttonSave.Text = "Save";
			this._buttonSave.UseVisualStyleBackColor = true;
			this._buttonSave.Click += new System.EventHandler(this.OnSave_Click);
			// 
			// _buttonCancel
			// 
			this._buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this._buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this._buttonCancel.Location = new System.Drawing.Point(597, 462);
			this._buttonCancel.Name = "_buttonCancel";
			this._buttonCancel.Size = new System.Drawing.Size(75, 23);
			this._buttonCancel.TabIndex = 10;
			this._buttonCancel.Text = "Cancel";
			this._buttonCancel.UseVisualStyleBackColor = true;
			this._buttonCancel.Click += new System.EventHandler(this.OnCancel_Click);
			// 
			// _buttonAddGoods
			// 
			this._buttonAddGoods.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this._buttonAddGoods.Location = new System.Drawing.Point(12, 462);
			this._buttonAddGoods.Name = "_buttonAddGoods";
			this._buttonAddGoods.Size = new System.Drawing.Size(75, 23);
			this._buttonAddGoods.TabIndex = 6;
			this._buttonAddGoods.Text = "Add item";
			this._buttonAddGoods.UseVisualStyleBackColor = true;
			this._buttonAddGoods.Click += new System.EventHandler(this.OnAddGoods_Click);
			// 
			// _buttonDelete
			// 
			this._buttonDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this._buttonDelete.Location = new System.Drawing.Point(93, 462);
			this._buttonDelete.Name = "_buttonDelete";
			this._buttonDelete.Size = new System.Drawing.Size(75, 23);
			this._buttonDelete.TabIndex = 7;
			this._buttonDelete.Text = "Delete";
			this._buttonDelete.UseVisualStyleBackColor = true;
			this._buttonDelete.Click += new System.EventHandler(this.OnDelete_Click);
			// 
			// _textBoxSum
			// 
			this._textBoxSum.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this._textBoxSum.Location = new System.Drawing.Point(516, 436);
			this._textBoxSum.Name = "_textBoxSum";
			this._textBoxSum.ReadOnly = true;
			this._textBoxSum.Size = new System.Drawing.Size(156, 20);
			this._textBoxSum.TabIndex = 5;
			this._textBoxSum.TabStop = false;
			// 
			// _labelSum
			// 
			this._labelSum.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this._labelSum.AutoSize = true;
			this._labelSum.Location = new System.Drawing.Point(479, 439);
			this._labelSum.Name = "_labelSum";
			this._labelSum.Size = new System.Drawing.Size(31, 13);
			this._labelSum.TabIndex = 4;
			this._labelSum.Text = "Sum:";
			// 
			// _buttonPrint
			// 
			this._buttonPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this._buttonPrint.ContextMenuStrip = this._contextMenuPrint;
			this._buttonPrint.Location = new System.Drawing.Point(174, 462);
			this._buttonPrint.Name = "_buttonPrint";
			this._buttonPrint.Size = new System.Drawing.Size(75, 23);
			this._buttonPrint.TabIndex = 8;
			this._buttonPrint.Text = "Print";
			this._buttonPrint.UseVisualStyleBackColor = true;
			this._buttonPrint.Click += new System.EventHandler(this.OnPrint_Click);
			// 
			// _contextMenuPrint
			// 
			this._contextMenuPrint.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._toolStripMenuItemDesign});
			this._contextMenuPrint.Name = "_contextMenuPrint";
			this._contextMenuPrint.Size = new System.Drawing.Size(153, 48);
			// 
			// _toolStripMenuItemDesign
			// 
			this._toolStripMenuItemDesign.Name = "_toolStripMenuItemDesign";
			this._toolStripMenuItemDesign.Size = new System.Drawing.Size(152, 22);
			this._toolStripMenuItemDesign.Text = "Design";
			this._toolStripMenuItemDesign.Click += new System.EventHandler(this.OnDesign_Click);
			// 
			// SellForm
			// 
			this.AcceptButton = this._buttonSave;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this._buttonCancel;
			this.ClientSize = new System.Drawing.Size(684, 497);
			this.Controls.Add(this._buttonPrint);
			this.Controls.Add(this._labelSum);
			this.Controls.Add(this._textBoxSum);
			this.Controls.Add(this._buttonDelete);
			this.Controls.Add(this._buttonAddGoods);
			this.Controls.Add(this._buttonCancel);
			this.Controls.Add(this._buttonSave);
			this.Controls.Add(this._dataGridView);
			this.Controls.Add(this._buttonChangeAssistent);
			this.Controls.Add(this._textBoxAssistent);
			this.Controls.Add(this._labelAssistent);
			this.Name = "SellForm";
			this.ShowIcon = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Sell";
			this.Load += new System.EventHandler(this.SellForm_Load);
			((System.ComponentModel.ISupportInitialize)(this._dataGridView)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this._bindingSource)).EndInit();
			this._contextMenuPrint.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label _labelAssistent;
		private System.Windows.Forms.TextBox _textBoxAssistent;
		private System.Windows.Forms.Button _buttonChangeAssistent;
		private System.Windows.Forms.DataGridView _dataGridView;
		private System.Windows.Forms.Button _buttonSave;
		private System.Windows.Forms.Button _buttonCancel;
		private System.Windows.Forms.Button _buttonAddGoods;
		private System.Windows.Forms.Button _buttonDelete;
		private System.Windows.Forms.TextBox _textBoxSum;
		private System.Windows.Forms.Label _labelSum;
		private System.Windows.Forms.Button _buttonPrint;
		private System.Windows.Forms.DataGridViewTextBoxColumn _columnArt;
		private System.Windows.Forms.DataGridViewTextBoxColumn _columnName;
		private System.Windows.Forms.DataGridViewButtonColumn _columnChangeGoods;
		private System.Windows.Forms.DataGridViewTextBoxColumn _columnAmount;
		private System.Windows.Forms.DataGridViewTextBoxColumn _columnSum;
		private System.Windows.Forms.BindingSource _bindingSource;
		private System.Windows.Forms.ContextMenuStrip _contextMenuPrint;
		private System.Windows.Forms.ToolStripMenuItem _toolStripMenuItemDesign;
	}
}
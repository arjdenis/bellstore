﻿namespace Arefjev.Bell.UI.WinForms
{
	partial class ChecksForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this._buttonClose = new System.Windows.Forms.Button();
			this._dataGridView = new System.Windows.Forms.DataGridView();
			this._columnId = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this._columnDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this._columnAssistent = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this._bindingSource = new System.Windows.Forms.BindingSource(this.components);
			this._buttonAdd = new System.Windows.Forms.Button();
			this._buttonDelete = new System.Windows.Forms.Button();
			this._buttonEdit = new System.Windows.Forms.Button();
			this._buttonPrint = new System.Windows.Forms.Button();
			this._contextMenuStripPrint = new System.Windows.Forms.ContextMenuStrip(this.components);
			this._toolStripMenuItemDesign = new System.Windows.Forms.ToolStripMenuItem();
			((System.ComponentModel.ISupportInitialize)(this._dataGridView)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this._bindingSource)).BeginInit();
			this._contextMenuStripPrint.SuspendLayout();
			this.SuspendLayout();
			// 
			// _buttonClose
			// 
			this._buttonClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this._buttonClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this._buttonClose.Location = new System.Drawing.Point(521, 400);
			this._buttonClose.Name = "_buttonClose";
			this._buttonClose.Size = new System.Drawing.Size(75, 23);
			this._buttonClose.TabIndex = 5;
			this._buttonClose.Text = "Close";
			this._buttonClose.UseVisualStyleBackColor = true;
			this._buttonClose.Click += new System.EventHandler(this.OnClose_Click);
			// 
			// _dataGridView
			// 
			this._dataGridView.AllowUserToAddRows = false;
			this._dataGridView.AllowUserToDeleteRows = false;
			this._dataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._dataGridView.AutoGenerateColumns = false;
			this._dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this._dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._columnId,
            this._columnDate,
            this._columnAssistent});
			this._dataGridView.DataSource = this._bindingSource;
			this._dataGridView.Location = new System.Drawing.Point(12, 12);
			this._dataGridView.Name = "_dataGridView";
			this._dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this._dataGridView.Size = new System.Drawing.Size(584, 382);
			this._dataGridView.TabIndex = 0;
			// 
			// _columnId
			// 
			this._columnId.DataPropertyName = "Id";
			this._columnId.HeaderText = "Id";
			this._columnId.MinimumWidth = 50;
			this._columnId.Name = "_columnId";
			this._columnId.ReadOnly = true;
			// 
			// _columnDate
			// 
			this._columnDate.DataPropertyName = "SellDate";
			this._columnDate.HeaderText = "Date";
			this._columnDate.MinimumWidth = 50;
			this._columnDate.Name = "_columnDate";
			this._columnDate.ReadOnly = true;
			this._columnDate.Width = 120;
			// 
			// _columnAssistent
			// 
			this._columnAssistent.DataPropertyName = "AssistentFullName";
			this._columnAssistent.HeaderText = "Assistent";
			this._columnAssistent.MinimumWidth = 50;
			this._columnAssistent.Name = "_columnAssistent";
			this._columnAssistent.ReadOnly = true;
			this._columnAssistent.Width = 300;
			// 
			// _buttonAdd
			// 
			this._buttonAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this._buttonAdd.Location = new System.Drawing.Point(12, 400);
			this._buttonAdd.Name = "_buttonAdd";
			this._buttonAdd.Size = new System.Drawing.Size(75, 23);
			this._buttonAdd.TabIndex = 1;
			this._buttonAdd.Text = "Add check";
			this._buttonAdd.UseVisualStyleBackColor = true;
			this._buttonAdd.Click += new System.EventHandler(this.OnAdd_Click);
			// 
			// _buttonDelete
			// 
			this._buttonDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this._buttonDelete.Location = new System.Drawing.Point(93, 400);
			this._buttonDelete.Name = "_buttonDelete";
			this._buttonDelete.Size = new System.Drawing.Size(75, 23);
			this._buttonDelete.TabIndex = 2;
			this._buttonDelete.Text = "Delete";
			this._buttonDelete.UseVisualStyleBackColor = true;
			this._buttonDelete.Click += new System.EventHandler(this.OnDelete_Click);
			// 
			// _buttonEdit
			// 
			this._buttonEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this._buttonEdit.Location = new System.Drawing.Point(174, 400);
			this._buttonEdit.Name = "_buttonEdit";
			this._buttonEdit.Size = new System.Drawing.Size(75, 23);
			this._buttonEdit.TabIndex = 3;
			this._buttonEdit.Text = "Edit";
			this._buttonEdit.UseVisualStyleBackColor = true;
			this._buttonEdit.Click += new System.EventHandler(this.OnEdit_Click);
			// 
			// _buttonPrint
			// 
			this._buttonPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this._buttonPrint.ContextMenuStrip = this._contextMenuStripPrint;
			this._buttonPrint.Location = new System.Drawing.Point(255, 400);
			this._buttonPrint.Name = "_buttonPrint";
			this._buttonPrint.Size = new System.Drawing.Size(75, 23);
			this._buttonPrint.TabIndex = 4;
			this._buttonPrint.Text = "Print";
			this._buttonPrint.UseVisualStyleBackColor = true;
			this._buttonPrint.Click += new System.EventHandler(this.OnPrint_Click);
			// 
			// _contextMenuStripPrint
			// 
			this._contextMenuStripPrint.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._toolStripMenuItemDesign});
			this._contextMenuStripPrint.Name = "_contextMenuStripPrint";
			this._contextMenuStripPrint.Size = new System.Drawing.Size(153, 48);
			// 
			// _toolStripMenuItemDesign
			// 
			this._toolStripMenuItemDesign.Name = "_toolStripMenuItemDesign";
			this._toolStripMenuItemDesign.Size = new System.Drawing.Size(152, 22);
			this._toolStripMenuItemDesign.Text = "Design";
			this._toolStripMenuItemDesign.Click += new System.EventHandler(this.OnDesign_Click);
			// 
			// ChecksForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this._buttonClose;
			this.ClientSize = new System.Drawing.Size(608, 435);
			this.Controls.Add(this._buttonPrint);
			this.Controls.Add(this._buttonEdit);
			this.Controls.Add(this._buttonDelete);
			this.Controls.Add(this._buttonAdd);
			this.Controls.Add(this._dataGridView);
			this.Controls.Add(this._buttonClose);
			this.Name = "ChecksForm";
			this.ShowIcon = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "ChecksForm";
			((System.ComponentModel.ISupportInitialize)(this._dataGridView)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this._bindingSource)).EndInit();
			this._contextMenuStripPrint.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion
		private System.Windows.Forms.Button _buttonClose;
		private System.Windows.Forms.DataGridView _dataGridView;
		private System.Windows.Forms.Button _buttonAdd;
		private System.Windows.Forms.Button _buttonDelete;
		private System.Windows.Forms.Button _buttonEdit;
		private System.Windows.Forms.BindingSource _bindingSource;
		private System.Windows.Forms.DataGridViewTextBoxColumn _columnId;
		private System.Windows.Forms.DataGridViewTextBoxColumn _columnDate;
		private System.Windows.Forms.DataGridViewTextBoxColumn _columnAssistent;
		private System.Windows.Forms.Button _buttonPrint;
		private System.Windows.Forms.ContextMenuStrip _contextMenuStripPrint;
		private System.Windows.Forms.ToolStripMenuItem _toolStripMenuItemDesign;
	}
}
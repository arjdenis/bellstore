﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Arefjev.Bell.Storage.BL;

namespace Arefjev.Bell.UI.WinForms
{
	/// <summary>
	/// Сервис отчетов на базе FastReport.
	/// </summary>
	public sealed class FastReportService : IReportingService
	{
		private readonly Dictionary<string, IEnumerable> _dataSources = new Dictionary<string, IEnumerable>();

		private readonly Dictionary<string, object> _parameters = new Dictionary<string, object>();

		/// <summary>
		/// Регистрирует данные для отчета.
		/// </summary>
		/// <param name="dataName">Имя источника.</param>
		/// <param name="data">Перечисление данных.</param>
		public void RegisterData(string dataName, IEnumerable data)
		{
			_dataSources[dataName] = data;
		}

		public void SetParameter(string paramName, object value)
		{
			_parameters[paramName] = value;
		}

		/// <summary>
		/// Отображает дизайнер отчета.
		/// </summary>
		/// <param name="reportPath">Путь к шаблону отчета.</param>
		public void ShowDesigner(string reportPath)
		{
			var report = Prepare(reportPath);
			report.Design();
		}

		/// <summary>
		/// Отображает отчет.
		/// </summary>
		/// <param name="reportPath">Путь к шаблону отчета.</param>
		public void ShowReport(string reportPath)
		{
			var report = Prepare(reportPath);
			report.Show();
		}

		private FastReport.Report Prepare(string reportPath)
		{
			var report = new FastReport.Report();

			foreach (var de in _dataSources)
			{
				report.RegisterData(de.Value, de.Key);
				report.GetDataSource(de.Key).Enabled = true;
			}

			if (File.Exists(reportPath))
				report.Load(reportPath);

			foreach (var de in _parameters)
			{
				report.SetParameterValue(de.Key, de.Value);
			}

			return report;
		}
	}
}

﻿namespace Arefjev.Bell.UI.WinForms
{
	partial class AssistentsForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this._dataGridView = new System.Windows.Forms.DataGridView();
			this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.fullNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.hireDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dismissionDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.passportIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this._columnIsDismissed = new System.Windows.Forms.DataGridViewCheckBoxColumn();
			this._bindingSource = new System.Windows.Forms.BindingSource(this.components);
			this._buttonClose = new System.Windows.Forms.Button();
			this._buttonHire = new System.Windows.Forms.Button();
			this._buttonDismit = new System.Windows.Forms.Button();
			this._buttonSave = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this._dataGridView)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this._bindingSource)).BeginInit();
			this.SuspendLayout();
			// 
			// _dataGridView
			// 
			this._dataGridView.AllowUserToAddRows = false;
			this._dataGridView.AllowUserToDeleteRows = false;
			this._dataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._dataGridView.AutoGenerateColumns = false;
			this._dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this._dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.fullNameDataGridViewTextBoxColumn,
            this.hireDateDataGridViewTextBoxColumn,
            this.dismissionDateDataGridViewTextBoxColumn,
            this.passportIdDataGridViewTextBoxColumn,
            this._columnIsDismissed});
			this._dataGridView.DataSource = this._bindingSource;
			this._dataGridView.Location = new System.Drawing.Point(12, 12);
			this._dataGridView.Name = "_dataGridView";
			this._dataGridView.ReadOnly = true;
			this._dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this._dataGridView.Size = new System.Drawing.Size(667, 358);
			this._dataGridView.TabIndex = 0;
			// 
			// idDataGridViewTextBoxColumn
			// 
			this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
			this.idDataGridViewTextBoxColumn.HeaderText = "Id";
			this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
			this.idDataGridViewTextBoxColumn.ReadOnly = true;
			this.idDataGridViewTextBoxColumn.Visible = false;
			// 
			// fullNameDataGridViewTextBoxColumn
			// 
			this.fullNameDataGridViewTextBoxColumn.DataPropertyName = "FullName";
			this.fullNameDataGridViewTextBoxColumn.HeaderText = "FullName";
			this.fullNameDataGridViewTextBoxColumn.MinimumWidth = 50;
			this.fullNameDataGridViewTextBoxColumn.Name = "fullNameDataGridViewTextBoxColumn";
			this.fullNameDataGridViewTextBoxColumn.ReadOnly = true;
			this.fullNameDataGridViewTextBoxColumn.Width = 200;
			// 
			// hireDateDataGridViewTextBoxColumn
			// 
			this.hireDateDataGridViewTextBoxColumn.DataPropertyName = "HireDate";
			this.hireDateDataGridViewTextBoxColumn.HeaderText = "HireDate";
			this.hireDateDataGridViewTextBoxColumn.MinimumWidth = 50;
			this.hireDateDataGridViewTextBoxColumn.Name = "hireDateDataGridViewTextBoxColumn";
			this.hireDateDataGridViewTextBoxColumn.ReadOnly = true;
			// 
			// dismissionDateDataGridViewTextBoxColumn
			// 
			this.dismissionDateDataGridViewTextBoxColumn.DataPropertyName = "DismissionDate";
			this.dismissionDateDataGridViewTextBoxColumn.HeaderText = "DismissionDate";
			this.dismissionDateDataGridViewTextBoxColumn.MinimumWidth = 50;
			this.dismissionDateDataGridViewTextBoxColumn.Name = "dismissionDateDataGridViewTextBoxColumn";
			this.dismissionDateDataGridViewTextBoxColumn.ReadOnly = true;
			// 
			// passportIdDataGridViewTextBoxColumn
			// 
			this.passportIdDataGridViewTextBoxColumn.DataPropertyName = "PassportId";
			this.passportIdDataGridViewTextBoxColumn.HeaderText = "PassportId";
			this.passportIdDataGridViewTextBoxColumn.MinimumWidth = 50;
			this.passportIdDataGridViewTextBoxColumn.Name = "passportIdDataGridViewTextBoxColumn";
			this.passportIdDataGridViewTextBoxColumn.ReadOnly = true;
			// 
			// _columnIsDismissed
			// 
			this._columnIsDismissed.DataPropertyName = "IsDismissed";
			this._columnIsDismissed.HeaderText = "Is dismissed";
			this._columnIsDismissed.MinimumWidth = 50;
			this._columnIsDismissed.Name = "_columnIsDismissed";
			this._columnIsDismissed.ReadOnly = true;
			this._columnIsDismissed.Resizable = System.Windows.Forms.DataGridViewTriState.True;
			this._columnIsDismissed.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
			this._columnIsDismissed.TrueValue = "";
			// 
			// _bindingSource
			// 
			this._bindingSource.AllowNew = false;
			this._bindingSource.DataSource = typeof(Arefjev.Bell.Storage.BL.Model.ShopAssistent);
			// 
			// _buttonClose
			// 
			this._buttonClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this._buttonClose.Location = new System.Drawing.Point(604, 376);
			this._buttonClose.Name = "_buttonClose";
			this._buttonClose.Size = new System.Drawing.Size(75, 23);
			this._buttonClose.TabIndex = 4;
			this._buttonClose.Text = "Close";
			this._buttonClose.UseVisualStyleBackColor = true;
			this._buttonClose.Click += new System.EventHandler(this.OnClose_Click);
			// 
			// _buttonHire
			// 
			this._buttonHire.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this._buttonHire.Location = new System.Drawing.Point(12, 376);
			this._buttonHire.Name = "_buttonHire";
			this._buttonHire.Size = new System.Drawing.Size(75, 23);
			this._buttonHire.TabIndex = 1;
			this._buttonHire.Text = "Hire";
			this._buttonHire.UseVisualStyleBackColor = true;
			this._buttonHire.Click += new System.EventHandler(this.OnHire_Click);
			// 
			// _buttonDismit
			// 
			this._buttonDismit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this._buttonDismit.Location = new System.Drawing.Point(93, 376);
			this._buttonDismit.Name = "_buttonDismit";
			this._buttonDismit.Size = new System.Drawing.Size(75, 23);
			this._buttonDismit.TabIndex = 2;
			this._buttonDismit.Text = "Dismit";
			this._buttonDismit.UseVisualStyleBackColor = true;
			this._buttonDismit.Click += new System.EventHandler(this.OnDismit_Click);
			// 
			// _buttonSave
			// 
			this._buttonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this._buttonSave.Location = new System.Drawing.Point(523, 376);
			this._buttonSave.Name = "_buttonSave";
			this._buttonSave.Size = new System.Drawing.Size(75, 23);
			this._buttonSave.TabIndex = 3;
			this._buttonSave.Text = "Save";
			this._buttonSave.UseVisualStyleBackColor = true;
			this._buttonSave.Click += new System.EventHandler(this.OnSave_Click);
			// 
			// AssistentsForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(691, 411);
			this.Controls.Add(this._buttonSave);
			this.Controls.Add(this._buttonDismit);
			this.Controls.Add(this._buttonHire);
			this.Controls.Add(this._buttonClose);
			this.Controls.Add(this._dataGridView);
			this.Name = "AssistentsForm";
			this.ShowIcon = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Assistents";
			((System.ComponentModel.ISupportInitialize)(this._dataGridView)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this._bindingSource)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.DataGridView _dataGridView;
		private System.Windows.Forms.Button _buttonClose;
		private System.Windows.Forms.BindingSource _bindingSource;
		private System.Windows.Forms.Button _buttonHire;
		private System.Windows.Forms.Button _buttonDismit;
		private System.Windows.Forms.Button _buttonSave;
		private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn fullNameDataGridViewTextBoxColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn hireDateDataGridViewTextBoxColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn dismissionDateDataGridViewTextBoxColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn passportIdDataGridViewTextBoxColumn;
		private System.Windows.Forms.DataGridViewCheckBoxColumn _columnIsDismissed;
	}
}
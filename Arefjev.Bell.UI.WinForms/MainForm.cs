﻿using System;
using System.Windows.Forms;
using Arefjev.Bell.Storage.BL.Presenter;
using Microsoft.Practices.Unity;

namespace Arefjev.Bell.UI.WinForms
{
	/// <summary>
	/// Главная форма приложения.
	/// </summary>
	public partial class MainForm : Form
	{
		private IUnityContainer _container;

		/// <summary>
		/// Инициализирует новый экземпляр.
		/// </summary>
		/// <param name="container">Ioc контейнер.</param>
		public MainForm(IUnityContainer container)
		{
			InitializeComponent();

			_container = container;
		}

		/// <summary>
		/// Выбран пункт меню Товары на складе.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnGoodsInStore_Click(object sender, EventArgs e)
		{
			var goodsInStorePresenter = _container.Resolve<GoodsInStorePresenter>();
			goodsInStorePresenter.Run();
		}

		/// <summary>
		/// Выбран пункт меню Товары.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnGoods_Click(object sender, EventArgs e)
		{
			var goodsPresenter = _container.Resolve<GoodsPresenter>();
			goodsPresenter.Run();
		}

		/// <summary>
		/// Выбран пункт меню Поступление товаров.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnGoodsIncoming_Click(object sender, EventArgs e)
		{
			var incomingPresenter = _container.Resolve<IncomingGoodsPresenter>();
			incomingPresenter.Run();
		}

		/// <summary>
		/// Выбран пункт меню Выход.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnExit_Click(object sender, EventArgs e)
		{
			Close();
		}

		/// <summary>
		/// Выбран пункт меню Продавцы.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnAssistents_Click(object sender, EventArgs e)
		{
			var assistentsPresenter = _container.Resolve<AssistentsPresenter>();
			assistentsPresenter.Run();
		}

		/// <summary>
		/// Выбран пункт меню Чеки.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnChecks_Click(object sender, EventArgs e)
		{
			var checksPresenter = _container.Resolve<ChecksPresenter>();
			checksPresenter.Run();
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Arefjev.Bell.Storage.BL;
using Arefjev.Bell.Storage.BL.View;

namespace Arefjev.Bell.UI.WinForms
{
	/// <summary>
	/// Редактор списка чеков.
	/// </summary>
	public partial class ChecksForm : Form, IChecksView
	{
		public ChecksForm()
		{
			InitializeComponent();

			_dataGridView.AutoGenerateColumns = false;
		}

		/// <summary>
		/// Команда сохранить.
		/// </summary>
		public event EventHandler SaveCommand = delegate { };

		/// <summary>
		/// Команда Добавить чек.
		/// </summary>
		public event EventHandler AddCheckCommand = delegate { };

		/// <summary>
		/// Команда Удалить чек.
		/// </summary>
		public event EventHandler RemoveCheckCommand = delegate { };

		/// <summary>
		/// Команда Редактировать чек.
		/// </summary>
		public event EventHandler EditCheckCommand = delegate { };

		/// <summary>
		/// Команда Печать отчета.
		/// </summary>
		public event EventHandler PrintCommand = delegate { };

		/// <summary>
		/// Команда Редактировать отчет.
		/// </summary>
		public event EventHandler DesignCommand = delegate { };

		/// <summary>
		/// Загрузить чеки.
		/// </summary>
		/// <param name="checks">Спискок чеков.</param>
		public void LoadChecks(IList<CheckViewModel> checks)
		{
			_bindingSource.DataSource = checks;
		}

		/// <summary>
		/// Возвращает выбранные чеки.
		/// </summary>
		public IList<CheckViewModel> SelectedChecks
		{
			get
			{
				return _dataGridView.SelectedRows
					.OfType<DataGridViewRow>()
					.Select(row => (CheckViewModel)row.DataBoundItem)
					.ToList();
			}
		}

		private void OnAdd_Click(object sender, EventArgs e)
		{
			AddCheckCommand(this, EventArgs.Empty);
		}

		private void OnDelete_Click(object sender, EventArgs e)
		{
			RemoveCheckCommand(this, EventArgs.Empty);
		}

		private void OnSave_Click(object sender, EventArgs e)
		{
			SaveCommand(this, EventArgs.Empty);
			Close();
		}

		private void OnClose_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void OnEdit_Click(object sender, EventArgs e)
		{
			EditCheckCommand(this, EventArgs.Empty);
		}

		bool IView.ShowDialog()
		{
			return ShowDialog() == DialogResult.OK;
		}

		private void OnPrint_Click(object sender, EventArgs e)
		{
			PrintCommand(this, EventArgs.Empty);
		}

		private void OnDesign_Click(object sender, EventArgs e)
		{
			DesignCommand(this, EventArgs.Empty);
		}
	}
}

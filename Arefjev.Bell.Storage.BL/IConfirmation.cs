﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arefjev.Bell.Storage.BL
{
	public interface IConfirmation
	{
		bool ShowQuestion(string question);
	}
}

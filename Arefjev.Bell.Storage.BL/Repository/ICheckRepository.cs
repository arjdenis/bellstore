﻿using System;
using System.Collections.Generic;
using Arefjev.Bell.Storage.BL.Model;

namespace Arefjev.Bell.Storage.BL.Repository
{
	/// <summary>
	/// Контракт репозитория для работы с чеками.
	/// </summary>
	public interface ICheckRepository
	{
		/// <summary>
		/// Возвращает перечисление всех чеков в БД.
		/// </summary>
		/// <returns>Перечисление всех чеков в БД.</returns>
		IEnumerable<Check> GetAll();

		IEnumerable<Check> GetAllWithItems(DateTime periodStart, DateTime periodEnd);

		/// <summary>
		/// Удаляет чеки соответствующие их идентификаторам.
		/// </summary>
		/// <param name="ids">Перечисление идентификатора.</param>
		void Remove(IEnumerable<int> ids);
	}
}

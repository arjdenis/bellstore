﻿using System.Collections;

namespace Arefjev.Bell.Storage.BL
{
	/// <summary>
	/// Контракт для просмотра отчетов.
	/// </summary>
	public interface IReportingService
	{
		/// <summary>
		/// Регистрирует данные для отчета.
		/// </summary>
		/// <param name="dataName">Имя источника.</param>
		/// <param name="data">Перечисление данных.</param>
		void RegisterData(string dataName, IEnumerable data);

		/// <summary>
		/// Устанавливает значение параметра отчета.
		/// </summary>
		/// <param name="paramName">Имя параметра.</param>
		/// <param name="value">Значение параметра.</param>
		void SetParameter(string paramName, object value);

		/// <summary>
		/// Отображает отчет.
		/// </summary>
		/// <param name="reportPath">Путь к шаблону отчета.</param>
		void ShowReport(string reportPath);

		/// <summary>
		/// Отображает дизайнер отчета.
		/// </summary>
		/// <param name="reportPath">Путь к шаблону отчета.</param>
		void ShowDesigner(string reportPath);
	}
}

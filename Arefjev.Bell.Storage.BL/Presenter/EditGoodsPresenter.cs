﻿using Arefjev.Bell.Storage.BL.Model;
using Arefjev.Bell.Storage.BL.View;

namespace Arefjev.Bell.Storage.BL.Presenter
{
	/// <summary>
	/// Логика работы редактора товара.
	/// </summary>
	public sealed class EditGoodsPresenter : IPresenter
	{
		private IStore _store;
		private IGoodsEditView _view;

		/// <summary>
		/// Инициализирует новый экземпляр.
		/// </summary>
		/// <param name="view">Редактор товара.</param>
		/// <param name="store">Контекст БД.</param>
		public EditGoodsPresenter(IGoodsEditView view, IStore store)
		{
			_store = store;
			_view = view;
		}

		/// <summary>
		/// Возвращает/устанавливает редактируемый товар.
		/// </summary>
		public Goods Goods { get; set; }

		/// <summary>
		/// Начало работы.
		/// </summary>
		public void Run()
		{
			_view.Goods = Goods;

			if (_view.ShowDialog())
			{
				_store.SaveChanges();
			}
		}
	}
}

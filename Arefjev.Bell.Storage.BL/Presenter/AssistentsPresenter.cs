﻿using System.ComponentModel;
using System.Linq;
using Arefjev.Bell.Storage.BL.Model;
using Arefjev.Bell.Storage.BL.StoreExtensions;
using Arefjev.Bell.Storage.BL.View;
using Microsoft.Practices.Unity;

namespace Arefjev.Bell.Storage.BL.Presenter
{
	/// <summary>
	/// Логика работы с продавцами.
	/// </summary>
	public sealed class AssistentsPresenter : IPresenter
	{
		private IStore _store;
		private IAssistentsView _view;
		private BindingList<ShopAssistent> _assistentsList;
		private IUnityContainer _container;
		private IConfirmation _confirmation;

		/// <summary>
		/// Инициализирует новый экземпляр.
		/// </summary>
		/// <param name="view">Редактор.</param>
		/// <param name="store">Контекст БД.</param>
		public AssistentsPresenter (IAssistentsView view, IStore store, IUnityContainer container, IConfirmation confirmation)
		{
			_view = view;
			_view.HireAssistentCommand += OnHireAssistentCommand;
			_view.DismitCommand += OnDismitCommand;
			_view.SaveCommand += OnSaveCommand;
			_view.Closed += OnClosed;

			_store = store;
			_container = container;
			_confirmation = confirmation;
		}

		/// <summary>
		/// Вызывается при закрытии редактора.
		/// Освобождает используемые ресурсы.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnClosed(object sender, System.EventArgs e)
		{
			_store.Dispose();
		}

		/// <summary>
		/// Вызывается при нажатии кнопки Сохранить.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnSaveCommand(object sender, System.EventArgs e)
		{
			_store.SaveChanges();
		}

		/// <summary>
		/// Вызывается при нажатии кнопки Уволить продавца.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnDismitCommand(object sender, System.EventArgs e)
		{
			var selectedAssistents = _view.SelectedAssistents;

			if (selectedAssistents.Count == 0)
				return;

			if (_confirmation.ShowQuestion("Dismiss selected assistents?"))
			{
				foreach (var assistent in selectedAssistents)
					assistent.Dismiss();
			}
		}

		/// <summary>
		/// Вызывается при нажатии кнопки Нанять продавца.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnHireAssistentCommand(object sender, System.EventArgs e)
		{
			var hireAssistentPresenter = _container.Resolve<AssistentHirePresenter>();

			hireAssistentPresenter.Run();

			var hiredAssistent = hireAssistentPresenter.Assistent;

			if (hiredAssistent == null)
				return;

			_store.Assistents.Add(hiredAssistent);
			_assistentsList.Add(hiredAssistent);
		}

		/// <summary>
		/// Начало работы.
		/// </summary>
		public void Run()
		{
			_assistentsList = new BindingList<ShopAssistent>(_store.Assistents
				.GetWorkingAssistents());
			_view.LoadAssistents(_assistentsList);
			
            _view.ShowDialog();
		}

		public ShopAssistent SelectedAssistent
		{
			get { return _view.SelectedAssistents.FirstOrDefault(); }
		}
	}
}

﻿using System;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using Arefjev.Bell.Storage.BL.Model;
using Arefjev.Bell.Storage.BL.View;
using Microsoft.Practices.Unity;

namespace Arefjev.Bell.Storage.BL.Presenter
{
	/// <summary>
	/// Логика работы с чеком.
	/// </summary>
	public sealed class SellPresenter : IPresenter
	{
		private IStore _store;
		private ISellView _view;
		private IConfirmation _confirmation;
		private IUnityContainer _container;
		private Check _check;
		private IReportingService _report;

		/// <summary>
		/// Инициализирует новый экземпляр.
		/// </summary>
		/// <param name="view">Редактор чека.</param>
		/// <param name="store">Контекст БД.</param>
		/// <param name="confirmation">Диалог подтверждения.</param>
		/// <param name="container">Контейнер.</param>
		public SellPresenter(ISellView view, IStore store, IConfirmation confirmation, IReportingService report, IUnityContainer container)
		{
			_view = view;
			_view.AddGoodsCommand += OnAddGoodsCommand;
			_view.Closed += OnClosed;
			_view.PrintCheckCommand += OnPrintCheckCommand;
			_view.RemoveGoodsCommand += OnRemoveGoodsCommand;
			_view.SaveCommand += OnSaveCommand;
			_view.SelectAssistentCommand += OnSelectAssistentCommand;
			_view.SelectGoodsCommand += OnSelectGoodsCommand;
			_view.DesignCommand += OnDesignCommand;
			_store = store;
			_confirmation = confirmation;
			_report = report;
			_container = container;
		}

		/// <summary>
		/// Команда редактирования отчета.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnDesignCommand(object sender, EventArgs e)
		{
			_report.RegisterData("Check", new[] { _check });
			_report.RegisterData("CheckItems", _check.Items);
			_report.RegisterData("Goods", _check.Items.Select(i => i.Goods));
			_report.ShowDesigner("Reports/Check.frx");
		}

		/// <summary>
		/// Команда выбора товара.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnSelectGoodsCommand(object sender, EventArgs e)
		{
			var selectedItem = _view.SelectedItems.FirstOrDefault();

			if (selectedItem == null)
				return;

			var goodsInStoreView = _container.Resolve<IGoodsInStoreView>();
			goodsInStoreView.LoadGoods(_store.Goods.ToList());
			
			if (goodsInStoreView.ShowDialog())
			{
				var selectedGoods = goodsInStoreView.SelectedGoods.FirstOrDefault();

				if (selectedGoods == null)
					return;

                selectedItem.ChangeGoods(selectedGoods);
            }
		}

		/// <summary>
		/// Команда Выбрать продавца.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnSelectAssistentCommand(object sender, EventArgs e)
		{
			var assistentsPresenter = _container.Resolve<SelectAssistentPresenter>();
			assistentsPresenter.SelectedAssistent = _check.ShopAssistent;
            assistentsPresenter.Run();

			var selectedAssistent = assistentsPresenter.SelectedAssistent;

			if (selectedAssistent == null)
				return;

			selectedAssistent = _store.Assistents.Find(selectedAssistent.Id);
			_view.Sell.ChangeAssistent(selectedAssistent);
		}

		/// <summary>
		/// Команда Сохранить.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnSaveCommand(object sender, EventArgs e)
		{
			_store.SaveChanges();
		}

		/// <summary>
		/// Команда Удалить элемент чека.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnRemoveGoodsCommand(object sender, EventArgs e)
		{
			var selectedItems = _view.SelectedItems;

			if (selectedItems.Count == 0)
				return;

			if (_confirmation.ShowQuestion("Remove selected items?"))
			{
				foreach (var item in selectedItems)
				{
					_check.Items.Remove(item.Item);
				}
			}
		}

		/// <summary>
		/// Команда Печать чека.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnPrintCheckCommand(object sender, EventArgs e)
		{
			_report.RegisterData("Check", new[] { _check });
			_report.RegisterData("CheckItems", _check.Items);
			_report.RegisterData("Goods", _check.Items.Select(i => i.Goods));
			_report.ShowReport("Reports/Check.frx");
		}

		/// <summary>
		/// Вызывается при закрытии редактора.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnClosed(object sender, EventArgs e)
		{
			_store.Dispose();
		}

		/// <summary>
		/// Команда Добавить элемент чека.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnAddGoodsCommand(object sender, EventArgs e)
		{
			var selectGoodsPresenter = _container.Resolve<GoodsInStorePresenter>();
			selectGoodsPresenter.Run();

			var selectedGoods = selectGoodsPresenter.SelectedGoods;

			if (selectedGoods == null)
				return;

			var checkItem = new CheckItem();

			checkItem.GoodsId = selectedGoods.Id;
			checkItem.Goods = _store.Goods.Find(checkItem.GoodsId);
			_check.Items.Add(checkItem);
			_view.Items.Add(new SellItemViewModel(_view.Sell, checkItem));
		}

		/// <summary>
		/// Начало работы.
		/// </summary>
		public void Run()
		{
			if (CheckId > 0)
			{
				_check = _store.Checks
					.Where(c => c.Id == CheckId)
					.Include(c => c.Items.Select(i => i.Goods))
					.Include(c => c.ShopAssistent)
					.Single();
			}
			else
			{
				_check = new Check();
				_store.Checks.Add(_check);
			}

			_view.Sell = new CheckViewModel(_check);
			_view.Items = new BindingList<SellItemViewModel>(_check.Items.Select(i => new SellItemViewModel(_view.Sell, i)).ToList());
			_view.ShowDialog();
		}

		/// <summary>
		/// Возвращает/устанавливает модель чека.
		/// </summary>
		public int CheckId { get; set; }
	}
}

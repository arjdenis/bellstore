﻿using Arefjev.Bell.Storage.BL.Model;
using Arefjev.Bell.Storage.BL.View;

namespace Arefjev.Bell.Storage.BL.Presenter
{
	/// <summary>
	/// Логика работы добавления товара.
	/// </summary>
	public sealed class AddGoodsPresenter : IPresenter
	{
		private IGoodsEditView _view;

		/// <summary>
		/// Инициализирует новый экземпляр.
		/// </summary>
		/// <param name="view">Редактор товара.</param>
		/// <param name="store">Контекст БД.</param>
		public AddGoodsPresenter(IGoodsEditView view)
		{
			_view = view;
		}

		/// <summary>
		/// Начало работы.
		/// </summary>
		public void Run()
		{
			if (_view.ShowDialog())
			{
				Goods = _view.Goods;
			}
		}

		public Goods Goods { get; private set; }
	}
}

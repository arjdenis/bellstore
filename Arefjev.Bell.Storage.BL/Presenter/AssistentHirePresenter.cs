﻿using Arefjev.Bell.Storage.BL.Model;
using Arefjev.Bell.Storage.BL.View;

namespace Arefjev.Bell.Storage.BL.Presenter
{
	/// <summary>
	/// Логика приема на работу продавца.
	/// </summary>
	public sealed class AssistentHirePresenter : IPresenter
	{
		private IHireAssistentView _view;

		/// <summary>
		/// Инициализирует новый экземпляр.
		/// </summary>
		/// <param name="view">Форма приема на работу.</param>
		public AssistentHirePresenter(IHireAssistentView view)
		{
			_view = view;
		}

		/// <summary>
		/// Возвращает нового продавца, либо null, в случае отмены.
		/// </summary>
		public ShopAssistent Assistent { get; private set; }

		/// <summary>
		/// Начало работы.
		/// </summary>
		public void Run()
		{
			if (_view.ShowDialog())
			{
				Assistent = _view.Assistent;
			}
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Arefjev.Bell.Storage.BL.Model;
using Arefjev.Bell.Storage.BL.View;

namespace Arefjev.Bell.Storage.BL.Presenter
{
	public sealed class SelectAssistentPresenter : IPresenter
	{
		private ISelectAssistentView _view;
		private IStore _store;

		public SelectAssistentPresenter(ISelectAssistentView view, IStore store)
		{
			_view = view;
			_store = store;
		}

		public void Run()
		{
			_view.LoadAssistents(_store.Assistents.ToList());
			_view.Assistent = SelectedAssistent;

			if (_view.ShowDialog())
				SelectedAssistent = _view.Assistent;
		}

		public ShopAssistent SelectedAssistent { get; set; }
	}
}

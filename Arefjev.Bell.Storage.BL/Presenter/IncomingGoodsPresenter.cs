﻿using System;
using System.Linq;
using Arefjev.Bell.Storage.BL.View;

namespace Arefjev.Bell.Storage.BL.Presenter
{
	/// <summary>
	/// Логика редактора поступлений товара.
	/// </summary>
	public sealed class IncomingGoodsPresenter : IPresenter
	{
		private IStore _store;
		private IIncomingGoodsView _view;

		/// <summary>
		/// Инициализирует новый экземпляр.
		/// </summary>
		/// <param name="view">Редактор поступления товаров.</param>
		/// <param name="store">Контекст БД.</param>
		public IncomingGoodsPresenter(IIncomingGoodsView view, IStore store)
		{
			_view = view;
			_store = store;

			_view.Closed += OnClosed;
			_view.SaveCommand += OnSaveCommand;
		}

		/// <summary>
		/// Вызывается по команде Сохранить поступления.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnSaveCommand(object sender, EventArgs e)
		{
			var incoming = _view.IncomingGoods;

			if (!incoming.Any())
				return;

			foreach (var goods in incoming)
			{
				_store.GoodsStores.Add(goods);
			}

			_store.SaveChanges();
		}

		/// <summary>
		/// Вызывается при закрытии редактора.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnClosed(object sender, EventArgs e)
		{
			_store.Dispose();
		}

		/// <summary>
		/// Начало работы.
		/// </summary>
		public void Run()
		{
			_view.LoadGoods(_store.Goods.ToList());
			_view.ShowDialog();
		}
	}
}

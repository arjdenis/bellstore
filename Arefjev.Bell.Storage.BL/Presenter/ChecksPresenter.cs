﻿using System;
using System.ComponentModel;
using System.Linq;
using Arefjev.Bell.Storage.BL.Repository;
using Arefjev.Bell.Storage.BL.View;
using Microsoft.Practices.Unity;

namespace Arefjev.Bell.Storage.BL.Presenter
{
	/// <summary>
	/// Логика работы со списком чеков.
	/// </summary>
	public sealed class ChecksPresenter : IPresenter
	{
		private IChecksView _view;
		private IConfirmation _confirmation;
		private IUnityContainer _container;
		private ICheckRepository _repository;
		private IReportingService _report;

		/// <summary>
		/// Инициализирует новый экземпляр.
		/// </summary>
		/// <param name="view">Редактор списка чеков.</param>
		/// <param name="repository">Репозиторий для работы с чеками.</param>
		/// <param name="confirmation">Диалог подтверждения.</param>
		/// <param name="container">Контейнер.</param>
		public ChecksPresenter(IChecksView view, ICheckRepository repository, IConfirmation confirmation, IReportingService report, IUnityContainer container)
		{
			_view = view;
			_view.AddCheckCommand += OnAddCheckCommand;
			_view.RemoveCheckCommand += OnRemoveCheckCommand;
			_view.EditCheckCommand += OnEditCheckCommand;
			_view.PrintCommand += OnPrintCommand;
			_view.DesignCommand += OnDesignCommand;
			_repository = repository;
			_confirmation = confirmation;
			_report = report;
			_container = container;
		}

		/// <summary>
		/// Команда Дизайн отчета.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnDesignCommand(object sender, EventArgs e)
		{
			var now = DateTime.UtcNow;
			var periodStart = new DateTime(now.Year, 1, 1);
			var periodEnd = new DateTime(now.Year, now.Month, DateTime.DaysInMonth(now.Year, now.Month));
			var checks = _repository.GetAllWithItems(periodStart, periodEnd);

			_report.RegisterData("Checks", checks);
			_report.SetParameter("PeriodStart", periodStart);
			_report.SetParameter("PeriodEnd", periodEnd);
			_report.ShowDesigner("Reports/Sells.frx");
		}

		/// <summary>
		/// Команда Печать отчета.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnPrintCommand(object sender, EventArgs e)
		{
			var now = DateTime.UtcNow;
			var periodStart = new DateTime(now.Year, 1, 1);
			var periodEnd = new DateTime(now.Year, now.Month, DateTime.DaysInMonth(now.Year, now.Month));
			var checks = _repository.GetAllWithItems(periodStart, periodEnd);

			_report.RegisterData("Checks", checks);
			_report.SetParameter("PeriodStart", periodStart);
			_report.SetParameter("PeriodEnd", periodEnd);
			_report.ShowReport("Reports/Sells.frx");
		}

		/// <summary>
		/// Команда Редактирование чека.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnEditCheckCommand(object sender, EventArgs e)
		{
			var selectedCheck = _view.SelectedChecks.FirstOrDefault();

			if (selectedCheck == null)
				return;

			var sellPresenter = _container.Resolve<SellPresenter>();
			sellPresenter.CheckId = selectedCheck.Id;
            sellPresenter.Run();

			LoadChecks();
		}

		/// <summary>
		/// Команда Удаление чека.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnRemoveCheckCommand(object sender, EventArgs e)
		{
			var selectedChecks = _view.SelectedChecks;

			if (selectedChecks.Count == 0)
				return;

			if (_confirmation.ShowQuestion("Delete selected checks?"))
			{
				_repository.Remove(selectedChecks.Select(c => c.Id));
				LoadChecks();
			}
		}

		/// <summary>
		/// Команда Добавить чек.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnAddCheckCommand(object sender, EventArgs e)
		{
			var sellPresenter = _container.Resolve<SellPresenter>();
			sellPresenter.Run();

			LoadChecks();
		}

		/// <summary>
		/// Начало работы.
		/// </summary>
		public void Run()
		{
			LoadChecks();
            _view.ShowDialog();
		}

		private void LoadChecks()
		{
			var checkList = new BindingList<CheckViewModel>(_repository
				.GetAll()
				.Select(c => new CheckViewModel(c)).ToList());
			_view.LoadChecks(checkList);
		}
	}
}

﻿using System.Data.Entity;
using System.Linq;
using Arefjev.Bell.Storage.BL.Model;
using Arefjev.Bell.Storage.BL.View;

namespace Arefjev.Bell.Storage.BL.Presenter
{
	/// <summary>
	/// Логика работы просмотра списка товаров.
	/// </summary>
	public sealed class GoodsInStorePresenter : IPresenter
	{
		private IStore _store;
		private IGoodsInStoreView _view;

		/// <summary>
		/// Инициализирует новый экземпляр.
		/// </summary>
		/// <param name="view">Представление товаров.</param>
		/// <param name="store">Контекст БД.</param>
		public GoodsInStorePresenter(IGoodsInStoreView view, IStore store)
		{
			_view = view;
			_store = store;
			_view.Closed += OnView_Closed;
		}

		/// <summary>
		/// Вызывается при закрытии представления.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnView_Closed(object sender, System.EventArgs e)
		{
			_store.Dispose();
		}

		/// <summary>
		/// Начало работы.
		/// </summary>
		public void Run()
		{
			_view.LoadGoods(_store.Goods
				.Include(g => g.GoodsInStore)
				.Include(g => g.Sells)
				.ToList());

			if (_view.ShowDialog())
			{
				SelectedGoods = _view.SelectedGoods.FirstOrDefault();
			}
		}

		public Goods SelectedGoods { get; set; }
	}
}

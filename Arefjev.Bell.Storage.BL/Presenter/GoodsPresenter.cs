﻿using System;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using Arefjev.Bell.Storage.BL.Model;
using Arefjev.Bell.Storage.BL.View;
using Microsoft.Practices.Unity;

namespace Arefjev.Bell.Storage.BL.Presenter
{
	/// <summary>
	/// Логика редактора списка товаров.
	/// </summary>
	public sealed class GoodsPresenter : IPresenter
	{
		private IStore _store;
		private IGoodsView _view;
		private IUnityContainer _container;
		private IConfirmation _confirmation;
		private BindingList<Goods> _goodsList;

		/// <summary>
		/// Инициализирует новый экземпляр.
		/// </summary>
		/// <param name="container">IoC контейнер.</param>
		/// <param name="view">Редактор товаров.</param>
		/// <param name="store">Контекст БД.</param>
		public GoodsPresenter(IUnityContainer container, IGoodsView view, IStore store, IConfirmation confirmation)
		{
			_view = view;
			_store = store;
			_container = container;
			_confirmation = confirmation;

			_view.Closed += OnClosed;
			_view.AddGoodsCommand += OnAddGoodsCommand;
			_view.EditGoodsCommand += OnEditGoodsCommand;
			_view.DeleteGoodsCommand += OnDeleteGoodsommand;
			_view.SaveCommand += OnSaveCommand;
		}

		private void OnSaveCommand(object sender, EventArgs e)
		{
			_store.SaveChanges();
		}

		/// <summary>
		/// Вызывается при удалении товара.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnDeleteGoodsommand(object sender, EventArgs e)
		{
			if (!_view.SelectedGoods.Any())
				return;

			if (_confirmation.ShowQuestion("Delete selected goods?"))
			{
				foreach (var goods in _view.SelectedGoods)
				{
					_store.Goods.Remove(goods);
					_goodsList.Remove(goods);
				}
			}
		}

		/// <summary>
		/// Вызывается при редактировании товара.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnEditGoodsCommand(object sender, EventArgs e)
		{
			var selectedGoods = _view.SelectedGoods.FirstOrDefault();

			if (selectedGoods == null)
				return;

			var addGoodsPresenter = _container.Resolve<EditGoodsPresenter>();

			addGoodsPresenter.Goods = selectedGoods;
			addGoodsPresenter.Run();
		}

		/// <summary>
		/// Вызывается при добавлении нового товара.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnAddGoodsCommand(object sender, EventArgs e)
		{
			var addGoodsPresenter = _container.Resolve<AddGoodsPresenter>();

			addGoodsPresenter.Run();

			var addedGoods = addGoodsPresenter.Goods;

			if (addedGoods == null)
				return;

			_store.Goods.Add(addedGoods);
			_goodsList.Add(addGoodsPresenter.Goods);
		}

		/// <summary>
		/// Вызывается закрытии редактора.
		/// </summary>
		/// <param name="sender">Отправитель события.</param>
		/// <param name="e">Параметры события.</param>
		private void OnClosed(object sender, EventArgs e)
		{
			_store.Dispose();
		}

		/// <summary>
		/// Начало работы.
		/// </summary>
		public void Run()
		{
			_goodsList = new BindingList<Goods>(
				_store.Goods
				.ToList());

            _view.LoadGoods(_goodsList);

			_view.ShowDialog();
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arefjev.Bell.Storage.BL.Model
{
	/// <summary>
	/// Товар на складе.
	/// </summary>
	public class GoodsStore
	{
		/// <summary>
		/// Идентификатор.
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// Товар.
		/// </summary>
		public Goods Goods { get; set; }

		/// <summary>
		/// Количество товара на складе.
		/// </summary>
		public decimal AmountOfGoods { get; set; }
	}
}

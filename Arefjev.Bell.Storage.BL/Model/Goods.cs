﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arefjev.Bell.Storage.BL.Model
{
	/// <summary>
	/// Справочник Товар.
	/// </summary>
	public class Goods
	{
		public Goods()
		{
			Unit = "шт";
		}

		/// <summary>
		/// Идентификатор товара.
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// Артикул товара.
		/// </summary>
		public string Article { get; set; }

		/// <summary>
		/// Наименование товара.
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Единица измерения товара.
		/// </summary>
		public string Unit { get; set; }

		/// <summary>
		/// Цена единицы товара.
		/// </summary>
		public decimal Price { get; set; }

		/// <summary>
		/// Товар на складе.
		/// </summary>
		public List<GoodsStore> GoodsInStore { get; set; }

		/// <summary>
		/// Список продаж этого товара.
		/// </summary>
		public List<CheckItem> Sells { get; set; }

		public decimal AmountOfGoods
		{
			get
			{
				if (GoodsInStore == null || Sells ==null)
					return 0m;

				var incoming = GoodsInStore.Sum(gis => gis.AmountOfGoods);
				var solt = Sells.Sum(sl => sl.AmountOfGoods);

				return incoming - solt;
			}
		}
	}
}

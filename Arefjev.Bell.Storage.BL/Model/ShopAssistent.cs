﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Arefjev.Bell.Storage.BL.Model
{
	/// <summary>
	/// Модель продавца.
	/// </summary>
	public class ShopAssistent : INotifyPropertyChanged
	{
		/// <summary>
		/// Инициализирует новый экземпляр.
		/// Автоматически устанавливается дата принятия на работу.
		/// </summary>
		public ShopAssistent()
		{
			HireDate = DateTime.UtcNow.Date;
		}

		/// <summary>
		/// Идентификатор.
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// ФИО.
		/// </summary>
		public string FullName { get; set; }

		/// <summary>
		/// Дата приема на работу.
		/// </summary>
		public DateTime HireDate { get; set; }

		/// <summary>
		/// Дата увольнения.
		/// </summary>
		public DateTime? DismissionDate { get; set; }

		/// <summary>
		/// Номер пасспорта.
		/// </summary>
		public string PassportId { get; set; }

		/// <summary>
		/// Список продаж для этого продавца.
		/// </summary>
		public List<Check> Checks { get; set; }

		/// <summary>
		/// Возвращает true, если этот продавец был уволен, иначе false.
		/// </summary>
		/// <returns></returns>
		public bool IsDismissed
		{
			get { return DismissionDate != null; }
		}

		public event PropertyChangedEventHandler PropertyChanged = delegate { };

		/// <summary>
		/// Увольнение продавца.
		/// Устанавливает текущую дату увольнения.
		/// </summary>
		public void Dismiss()
		{
			DismissionDate = DateTime.UtcNow.Date;
			PropertyChanged(this, new PropertyChangedEventArgs("IsDismissed"));
        }
	}
}

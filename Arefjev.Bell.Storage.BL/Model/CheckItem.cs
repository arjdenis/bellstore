﻿namespace Arefjev.Bell.Storage.BL.Model
{
	/// <summary>
	/// Продажа товара.
	/// </summary>
	public class CheckItem
	{
		/// <summary>
		/// Идентивикатор.
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// Идентификатор продаваемого товара.
		/// </summary>
		public int GoodsId { get; set; }

		/// <summary>
		/// Продаваемый товар.
		/// </summary>
		public Goods Goods { get; set; }

		/// <summary>
		/// Количество продаваемого товара.
		/// </summary>
		public decimal AmountOfGoods { get; set; }

		/// <summary>
		/// Чек.
		/// </summary>
		public Check Check { get; set; }

		/// <summary>
		/// Возвращает сумму товара.
		/// </summary>
		public decimal ItemSum
		{
			get { return Goods == null ? 0m : Goods.Price * AmountOfGoods; }
		}
	}
}

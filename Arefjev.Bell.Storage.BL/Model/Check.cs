﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Arefjev.Bell.Storage.BL.Model
{
	/// <summary>
	/// Чек.
	/// </summary>
	public class Check
	{
		public Check()
		{
			SellDate = DateTime.UtcNow.Date;
			Items = new List<CheckItem>();
        }

		/// <summary>
		/// Идентификатор чека.
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// Продавец.
		/// </summary>
		public ShopAssistent ShopAssistent { get; set; }

		/// <summary>
		/// Дата и время продажи (в БД хранить в utc).
		/// </summary>
		public DateTime SellDate { get; set; }

		/// <summary>
		/// Список проданных товаров.
		/// </summary>
		public virtual List<CheckItem> Items { get; set; }

		/// <summary>
		/// Возвращает итоговую сумму чека.
		/// </summary>
		public decimal CheckSum
		{
			get
			{
				return Items == null
					? 0m
					: Items.Sum(i => i.ItemSum);
			}
		}
	}
}

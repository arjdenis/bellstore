﻿using System;
using System.Collections.Generic;

namespace Arefjev.Bell.Storage.BL.View
{
	/// <summary>
	/// Контракт редактора списка чеков.
	/// </summary>
	public interface IChecksView : IView
	{
		/// <summary>
		/// Загрузить чеки.
		/// </summary>
		/// <param name="checks">Спискок чеков.</param>
		void LoadChecks(IList<CheckViewModel> checks);

		/// <summary>
		/// Возвращает выбранные чеки.
		/// </summary>
		IList<CheckViewModel> SelectedChecks { get; }

		/// <summary>
		/// Команда Добавить чек.
		/// </summary>
		event EventHandler AddCheckCommand;

		/// <summary>
		/// Команда Удалить чек.
		/// </summary>
		event EventHandler RemoveCheckCommand;

		/// <summary>
		/// Команда Редактировать чек.
		/// </summary>
		event EventHandler EditCheckCommand;

		/// <summary>
		/// Команда Печать отчета.
		/// </summary>
		event EventHandler PrintCommand;

		/// <summary>
		/// Команда Редактировать отчет.
		/// </summary>
		event EventHandler DesignCommand;
	}
}

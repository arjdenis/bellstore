﻿using System.Collections.Generic;
using Arefjev.Bell.Storage.BL.Model;

namespace Arefjev.Bell.Storage.BL.View
{
	public interface ISelectAssistentView : IView
	{
		ShopAssistent Assistent { get; set; }

		void LoadAssistents(List<ShopAssistent> list);
	}
}

﻿using System;
using System.Collections.Generic;
using Arefjev.Bell.Storage.BL.Model;

namespace Arefjev.Bell.Storage.BL.View
{
	/// <summary>
	/// Контракт редактора продавцов.
	/// </summary>
	public interface IAssistentsView : IView
	{
		/// <summary>
		/// Загружает список продавцов
		/// </summary>
		/// <param name="assistents">Список продавцов.</param>
		void LoadAssistents(IList<ShopAssistent> assistents);

		/// <summary>
		/// Возвращает список выбранных продавцов.
		/// </summary>
		IList<ShopAssistent> SelectedAssistents { get; }

		/// <summary>
		/// Команда нанять продавца.
		/// </summary>
		event EventHandler HireAssistentCommand;

		/// <summary>
		/// Команда уволить продавца.
		/// </summary>
		event EventHandler DismitCommand;

		/// <summary>
		/// Команда сохранить изменения.
		/// </summary>
		event EventHandler SaveCommand;
	}
}

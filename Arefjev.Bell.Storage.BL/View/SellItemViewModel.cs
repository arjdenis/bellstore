﻿using System.ComponentModel;
using Arefjev.Bell.Storage.BL.Model;

namespace Arefjev.Bell.Storage.BL.View
{
	public sealed class SellItemViewModel : INotifyPropertyChanged
	{
		private CheckItem _checkItem;
		private CheckViewModel _parent;

		public SellItemViewModel(CheckViewModel parent, CheckItem checkItem)
		{
			_parent = parent;
			_checkItem = checkItem;
		}

		public CheckItem Item
		{
			get { return _checkItem; }
		}

		public void ChangeGoods(Goods goods)
		{
			if (_checkItem.Goods != null && goods != null && _checkItem.Goods.Id == goods.Id)
				return;

			_checkItem.Goods = goods;
			PropertyChanged(this, new PropertyChangedEventArgs("Article"));
			PropertyChanged(this, new PropertyChangedEventArgs("Name"));
			PropertyChanged(this, new PropertyChangedEventArgs("ItemSum"));
			_parent.SumChanged();
		}

		public string Article
		{
			get { return _checkItem.Goods.Article; }
		}

		public string Name
		{
			get { return _checkItem.Goods.Name; }
		}

		public decimal Amount
		{
			get { return _checkItem.AmountOfGoods; }
			set
			{
				if (Amount == value)
					return;

				_checkItem.AmountOfGoods = value;
				PropertyChanged(this, new PropertyChangedEventArgs("Amount"));
				PropertyChanged(this, new PropertyChangedEventArgs("ItemSum"));
				_parent.SumChanged();
			}
		}

		public decimal ItemSum
		{
			get { return _checkItem.ItemSum; }
		}

		public event PropertyChangedEventHandler PropertyChanged = delegate { };
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Arefjev.Bell.Storage.BL.Model;

namespace Arefjev.Bell.Storage.BL.View
{
	/// <summary>
	/// Контракт формы просмотра товаров.
	/// </summary>
	public interface IGoodsInStoreView : IView
	{
		void LoadGoods(IList<Goods> goods);

		IList<Goods> SelectedGoods { get; }
	}
}

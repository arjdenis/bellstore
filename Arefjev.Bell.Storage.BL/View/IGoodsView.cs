﻿using System;
using System.Collections.Generic;
using Arefjev.Bell.Storage.BL.Model;

namespace Arefjev.Bell.Storage.BL.View
{
	/// <summary>
	/// Контракт редактора справочника товаров.
	/// </summary>
	public interface IGoodsView : IView
	{
		/// <summary>
		/// Отображает товары.
		/// </summary>
		/// <param name="goods">Список товаров.</param>
		void LoadGoods(IList<Goods> goods);

		/// <summary>
		/// Команда добавления товара.
		/// </summary>
		event EventHandler AddGoodsCommand;

		/// <summary>
		/// Команда редактирования товара.
		/// </summary>
		event EventHandler EditGoodsCommand;

		/// <summary>
		/// Команда на удаление товара.
		/// </summary>
		event EventHandler DeleteGoodsCommand;

		/// <summary>
		/// Команда на сохранение изменений.
		/// </summary>
		event EventHandler SaveCommand;

		/// <summary>
		/// Возвращает перечисление выбранных товаров.
		/// </summary>
		IEnumerable<Goods> SelectedGoods { get; }
    }
}

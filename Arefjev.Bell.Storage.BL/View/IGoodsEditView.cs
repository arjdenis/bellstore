﻿using Arefjev.Bell.Storage.BL.Model;

namespace Arefjev.Bell.Storage.BL.View
{
	/// <summary>
	/// Контракт редактора свойств товара.
	/// </summary>
	public interface IGoodsEditView : IView
	{
		/// <summary>
		/// Возвращает/устанавливает модель редактируемого товара.
		/// </summary>
		Goods Goods { get; set; }
	}
}

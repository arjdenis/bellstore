﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Arefjev.Bell.Storage.BL.Model;

namespace Arefjev.Bell.Storage.BL.View
{
	/// <summary>
	/// Контракт редактора продавца при его найме.
	/// </summary>
	public interface IHireAssistentView : IView
	{
		/// <summary>
		/// Возвращает/устанавливает модель продавца.
		/// </summary>
		ShopAssistent Assistent { get; set; }
	}
}

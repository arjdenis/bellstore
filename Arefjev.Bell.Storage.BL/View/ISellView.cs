﻿using System;
using System.Collections.Generic;

namespace Arefjev.Bell.Storage.BL.View
{
	public interface ISellView : IView
	{
		CheckViewModel Sell { get; set; }
		IList<SellItemViewModel> Items { get; set; }
		IList<SellItemViewModel> SelectedItems { get; }

		event EventHandler AddGoodsCommand;
		event EventHandler RemoveGoodsCommand;
		event EventHandler PrintCheckCommand;
		event EventHandler SaveCommand;
		event EventHandler SelectAssistentCommand;
		event EventHandler SelectGoodsCommand;
		event EventHandler DesignCommand;
	}
}

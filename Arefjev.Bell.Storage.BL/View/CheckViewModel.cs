﻿using System;
using System.ComponentModel;
using Arefjev.Bell.Storage.BL.Model;

namespace Arefjev.Bell.Storage.BL.View
{
	/// <summary>
	/// Модель представления чека.
	/// </summary>
	public sealed class CheckViewModel : INotifyPropertyChanged
	{
		private const string NotSelected = "(not selected)";

        private Check _check;

		/// <summary>
		/// Инициализирует новый экземпляр.
		/// </summary>
		/// <param name="check">Модель чека.</param>
		public CheckViewModel(Check check)
		{
			_check = check;
		}

		/// <summary>
		/// Возвращает модель чека.
		/// </summary>
		public Check Check
		{
			get { return _check; }
		}

		/// <summary>
		/// Возвращает идентификатор чека.
		/// </summary>
		public int Id
		{
			get { return _check.Id; }
		}

		/// <summary>
		/// Возвращает локальную дату продажи чека.
		/// </summary>
		public DateTime SellDate
		{
			get { return _check.SellDate.ToLocalTime(); }
		}

		/// <summary>
		/// Возвращает ФИО продавца.
		/// </summary>
		public string AssistentFullName
		{
			get
			{
				return _check.ShopAssistent == null
				  ? NotSelected
				  : _check.ShopAssistent.FullName;
			}
		}

		public void ChangeAssistent(ShopAssistent assistent)
		{
			if (_check.ShopAssistent != null && _check.ShopAssistent.Id == assistent.Id)
				return;

			_check.ShopAssistent = assistent;
			PropertyChanged(this, new PropertyChangedEventArgs("AssistentFullName"));
		}

		public decimal Sum
		{
			get { return _check.CheckSum; }
		}

		public void SumChanged()
		{
			PropertyChanged(this, new PropertyChangedEventArgs("Sum"));
		}

		/// <summary>
		/// Уведомление об изменениях модели.
		/// </summary>
		public event PropertyChangedEventHandler PropertyChanged = delegate { };
	}
}

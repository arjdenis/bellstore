﻿using System;
using System.Collections.Generic;
using Arefjev.Bell.Storage.BL.Model;

namespace Arefjev.Bell.Storage.BL.View
{
	/// <summary>
	/// Контракт редактора поступления товаров.
	/// </summary>
	public interface IIncomingGoodsView : IView
	{
		/// <summary>
		/// Отображает товары.
		/// </summary>
		/// <param name="goods">Список товаров.</param>
		void LoadGoods(IList<Goods> goods);

		/// <summary>
		/// Возвращает перечисление поступлений товаров. 
		/// </summary>
		IEnumerable<GoodsStore> IncomingGoods { get; }

		/// <summary>
		/// Команда на сохранение поступлений.
		/// </summary>
		event EventHandler SaveCommand;
	}
}

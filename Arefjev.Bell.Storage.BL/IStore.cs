﻿using System;
using System.Data.Entity;
using Arefjev.Bell.Storage.BL.Model;

namespace Arefjev.Bell.Storage.BL
{
	public interface IStore : IDisposable
	{
		/// <summary>
		/// Доступ к данным продавцов.
		/// </summary>
		IDbSet<ShopAssistent> Assistents { get; }

		/// <summary>
		/// Доступ к данным чеков.
		/// </summary>
		IDbSet<Check> Checks { get; }

		/// <summary>
		/// Доступ к данным продаж товаров.
		/// </summary>
		IDbSet<CheckItem> CheckItems { get; }

		/// <summary>
		/// Доступ к данным о товарах.
		/// </summary>
		IDbSet<Goods> Goods { get; }

		/// <summary>
		/// Доступ к данным о складе товаров.
		/// </summary>
		IDbSet<GoodsStore> GoodsStores { get; }

		int SaveChanges();
	}
}

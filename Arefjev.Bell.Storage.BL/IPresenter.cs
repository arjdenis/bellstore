﻿namespace Arefjev.Bell.Storage.BL
{
	/// <summary>
	/// Контракт для реализаии логики представлений.
	/// </summary>
	public interface IPresenter
	{
		/// <summary>
		/// Начало работы.
		/// </summary>
		void Run();
	}
}

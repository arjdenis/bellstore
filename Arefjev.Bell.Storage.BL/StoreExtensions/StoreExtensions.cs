﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Arefjev.Bell.Storage.BL.Model;

namespace Arefjev.Bell.Storage.BL.StoreExtensions
{
	public static class StoreExtensions
	{
		public static IList<ShopAssistent> GetWorkingAssistents(this IDbSet<ShopAssistent> assistents)
		{
			return assistents
				.Where(a => a.DismissionDate == null)
				.ToList();
		}
	}
}

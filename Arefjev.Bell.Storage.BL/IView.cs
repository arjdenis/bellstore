﻿using System;

namespace Arefjev.Bell.Storage.BL
{
	public interface IView
	{
		bool ShowDialog();
		void Close();

		event EventHandler Closed;
	}
}

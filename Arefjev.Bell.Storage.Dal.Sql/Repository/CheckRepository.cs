﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Arefjev.Bell.Storage.BL.Model;
using Arefjev.Bell.Storage.BL.Repository;
using Arefjev.Bell.Storage.Dal.Sql.Model;

namespace Arefjev.Bell.Storage.Dal.Sql.Repository
{
	/// <summary>
	/// SQL репозиторий для работы с чеками.
	/// </summary>
	public sealed class CheckRepository : ICheckRepository
	{
		/// <summary>
		/// Возвращает пречисление всех чеков в БД.
		/// </summary>
		/// <returns>Перечисление всех чеков.</returns>
		public IEnumerable<Check> GetAll()
		{
			using (var ctx = new StoreContext())
			{
				return ctx.Checks
					.Include(c => c.ShopAssistent)
					.ToList();
			}
		}

		/// <summary>
		/// Возвращает пречисление всех чеков в БД за указанный период.
		/// </summary>
		/// <param name="periodStart">Дата начала периода.</param>
		/// <param name="periodEnd">Дата завершения периода.</param>
		/// <returns>Перечисление всех чеков.</returns>
		public IEnumerable<Check> GetAllWithItems(DateTime periodStart, DateTime periodEnd)
		{
			using (var ctx = new StoreContext())
			{
				return ctx.Checks
					.Where(c => c.SellDate >= periodStart && c.SellDate <= periodEnd)
					.Include(c => c.ShopAssistent)
					.Include(c => c.Items)
					.Include(c => c.Items.Select(i => i.Goods))
					.ToList();
			}
		}

		/// <summary>
		/// Удаляет чеки соответствующие их идентификаторам.
		/// </summary>
		/// <param name="ids">Перечисление идентификатора.</param>
		public void Remove(IEnumerable<int> ids)
		{
			using (var ctx = new StoreContext())
			{
				foreach (var id in ids)
				{
					var check = ctx.Checks.Find(id);
					ctx.Checks.Remove(check);
                }
					
				ctx.SaveChanges();
			}
		}
	}
}

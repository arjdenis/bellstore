﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Arefjev.Bell.Storage.BL.Model;

namespace Arefjev.Bell.Storage.Dal.Sql.Model
{
	internal sealed class CheckConfiguration : EntityTypeConfiguration<Check>
	{
		internal CheckConfiguration()
		{
			HasRequired(c => c.ShopAssistent)
				.WithMany(a => a.Checks);

			HasMany(c => c.Items)
				.WithRequired(i => i.Check);
		}
	}
}

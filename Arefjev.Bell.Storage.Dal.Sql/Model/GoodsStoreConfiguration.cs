﻿using System.Data.Entity.ModelConfiguration;
using Arefjev.Bell.Storage.BL.Model;

namespace Arefjev.Bell.Storage.Dal.Sql.Model
{
	internal sealed class GoodsStoreConfiguration : EntityTypeConfiguration<GoodsStore>
	{
		internal GoodsStoreConfiguration()
		{
			Property(c => c.AmountOfGoods)
				.HasPrecision(18, 3);

			HasRequired(s => s.Goods)
				.WithMany(g => g.GoodsInStore);
		}
	}
}

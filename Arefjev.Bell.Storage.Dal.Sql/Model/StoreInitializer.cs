﻿using System.Linq;
using System.Collections.Generic;
using System.Data.Entity;
using Arefjev.Bell.Storage.BL.Model;
using System;

namespace Arefjev.Bell.Storage.Dal.Sql.Model
{
	/// <summary>
	/// Инициализатор БД.
	/// </summary>
	public sealed class StoreInitializer : DropCreateDatabaseIfModelChanges<StoreContext>
	{
		/// <summary>
		/// Вызывается после инициализации БД.
		/// Здесь происходит первоначальное наполнение БД данными.
		/// </summary>
		/// <param name="context"></param>
		protected override void Seed(StoreContext context)
		{
			try
			{

				foreach (var num in Enumerable.Range(1, 100))
				{
					context.Goods.Add(CreateGoods(string.Format("GDS{0}", num), string.Format("Goods{0}", num), 25 * num, num, 100.5m * num));
				}

				context.SaveChanges();

				foreach (var num in Enumerable.Range(1, 9))
				{
					context.Assistents.Add(CreateAssistent(string.Format("Assistent {0}", num), string.Format("{0}{0}-{0}{0}{0}{0}{0}{0}", num)));
				}

				context.SaveChanges();

				var now = DateTime.Now;

				foreach (var assistent in context.Assistents.ToList())
					foreach (var month in Enumerable.Range(1, 12))
						foreach (var num in Enumerable.Range(1, 5))
						{
							context.Checks.Add(CreateCheck(new DateTime(now.Year, month, num), assistent, context.Goods.Take(10).ToArray(), 1));
						}

				context.SaveChanges();
			}
			catch (Exception x)
			{

			}

			base.Seed(context);
		}

		/// <summary>
		/// Создает новый товар с приходом.
		/// </summary>
		/// <param name="art"></param>
		/// <param name="name"></param>
		/// <param name="price"></param>
		/// <param name="instore"></param>
		/// <param name="amount"></param>
		/// <returns></returns>
		private static Goods CreateGoods(string art, string name, decimal price, int instore, decimal amount)
		{
			return new Goods
			{
				Article = art,
				Name = name,
				Price = price,
				GoodsInStore = new List<GoodsStore>(
					Enumerable.Range(1, instore)
						.Select(i => new GoodsStore { AmountOfGoods = amount }))
			};
		}

		/// <summary>
		/// Создает нового продавца.
		/// </summary>
		/// <param name="fullname"></param>
		/// <param name="passport"></param>
		/// <returns></returns>
		private static ShopAssistent CreateAssistent(string fullname, string passport)
		{
			return new ShopAssistent
			{
				FullName = fullname,
				PassportId = passport
			};
		}

		/// <summary>
		/// Создает продажу товара.
		/// </summary>
		/// <param name="assistant"></param>
		/// <param name="goods"></param>
		/// <param name="amount"></param>
		/// <returns></returns>
		private static Check CreateCheck(DateTime sellDate, ShopAssistent assistant, Goods[] goods, decimal amount)
		{
			return new Check
			{
				ShopAssistent = assistant,
				SellDate = sellDate,
				Items = new List<CheckItem>(goods.Select(g => new CheckItem { Goods = g, AmountOfGoods = amount }))
			};
		}
	}
}

﻿using System.Data.Entity;
using Arefjev.Bell.Storage.BL;
using Arefjev.Bell.Storage.BL.Model;

namespace Arefjev.Bell.Storage.Dal.Sql.Model
{
	/// <summary>
	/// Контекст для доступа к БД.
	/// </summary>
	public class StoreContext : DbContext, IStore
	{
		/// <summary>
		/// Инициализирует новый экземпляр.
		/// </summary>
		public StoreContext() : this("StoreConnection")
		{
		}

		/// <summary>
		/// Инициализирует новый экземпляр.
		/// </summary>
		/// <param name="dbName">Имя БД или подключения.</param>
        public StoreContext(string dbName) : base(dbName)
		{
		}

		/// <summary>
		/// Доступ к данным продавцов.
		/// </summary>
		public IDbSet<ShopAssistent> Assistents { get; set; }

		/// <summary>
		/// Доступ к данным чеков.
		/// </summary>
		public IDbSet<Check> Checks { get; set; }

		/// <summary>
		/// Доступ к данным продаж товаров.
		/// </summary>
		public IDbSet<CheckItem> CheckItems { get; set; }

		/// <summary>
		/// Доступ к данным о товарах.
		/// </summary>
		public IDbSet<Goods> Goods { get; set; }

		/// <summary>
		/// Доступ к данным о складе товаров.
		/// </summary>
		public IDbSet<GoodsStore> GoodsStores { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Configurations.Add(new ShopAssistentConfiguration());
			modelBuilder.Configurations.Add(new CheckItemConfiguration());
			modelBuilder.Configurations.Add(new GoodsConfiguration());
			modelBuilder.Configurations.Add(new GoodsStoreConfiguration());
			modelBuilder.Configurations.Add(new CheckConfiguration());

			base.OnModelCreating(modelBuilder);
		}
	}
}

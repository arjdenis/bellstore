﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using Arefjev.Bell.Storage.BL.Model;

namespace Arefjev.Bell.Storage.Dal.Sql.Model
{
	internal sealed class GoodsConfiguration : EntityTypeConfiguration<Goods>
	{
		internal GoodsConfiguration()
		{
			Property(c => c.Name)
				.IsRequired()
				.HasMaxLength(255)
				.HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute()));

			Property(c => c.Article)
				.IsRequired()
				.HasMaxLength(30)
				.HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute { IsUnique = true }));

			HasMany(g => g.GoodsInStore)
				.WithRequired(g => g.Goods);

			HasMany(g => g.Sells)
				.WithRequired(ci => ci.Goods);
		}
	}
}

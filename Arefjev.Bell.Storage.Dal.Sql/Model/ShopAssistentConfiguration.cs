﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using Arefjev.Bell.Storage.BL.Model;

namespace Arefjev.Bell.Storage.Dal.Sql.Model
{
	internal sealed class ShopAssistentConfiguration : EntityTypeConfiguration<ShopAssistent>
	{
		internal ShopAssistentConfiguration()
		{
			Property(c => c.FullName)
				.IsRequired()
				.HasMaxLength(255);

			Property(c => c.PassportId)
				.IsRequired()
				.HasMaxLength(30)
				.HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute { IsUnique = true }));
        }
	}
}

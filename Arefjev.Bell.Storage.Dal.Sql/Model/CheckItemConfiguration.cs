﻿using System.Data.Entity.ModelConfiguration;
using Arefjev.Bell.Storage.BL.Model;

namespace Arefjev.Bell.Storage.Dal.Sql.Model
{
	internal sealed class CheckItemConfiguration : EntityTypeConfiguration<CheckItem>
	{
		internal CheckItemConfiguration()
		{
			Property(c => c.AmountOfGoods)
				.HasPrecision(18, 2);

			HasRequired(i => i.Check)
				.WithMany(c => c.Items);
		}
	}
}

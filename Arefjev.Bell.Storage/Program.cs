﻿using System;
using System.Data.Entity;
using System.Windows.Forms;
using Microsoft.Practices.Unity;
using Arefjev.Bell.Storage.BL;
using Arefjev.Bell.Storage.Dal.Sql.Model;
using Arefjev.Bell.UI.WinForms;
using Arefjev.Bell.Storage.BL.View;
using Arefjev.Bell.Storage.BL.Repository;
using Arefjev.Bell.Storage.Dal.Sql.Repository;

namespace Arefjev.Bell.Storage
{
	static class Program
	{
		static IUnityContainer container;

		/// <summary>
		/// Главная точка входа для приложения.
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			InitializeDatabase();

			container = CreateContainer();

			Application.Run(container.Resolve<MainForm>());
		}

		private static IUnityContainer CreateContainer()
		{
			var cnt = new UnityContainer();

			cnt
				.RegisterType<IStore>(new InjectionFactory(c => new StoreContext()))
				.RegisterType<ICheckRepository, CheckRepository>()

				.RegisterType<MainForm>(new ContainerControlledLifetimeManager())
				
				.RegisterType<IGoodsView, GoodsForm>()
				.RegisterType<IGoodsEditView, GoodsEditForm>()
				.RegisterType<IIncomingGoodsView, GoodsIncomingForm>()
				.RegisterType<IGoodsInStoreView, GoodsInStoreForm>()

				.RegisterType<IAssistentsView, AssistentsForm>()
				.RegisterType<IHireAssistentView, HireAssistentForm>()
				.RegisterType<ISelectAssistentView, SelectAssistentForm>()

				.RegisterType<IChecksView, ChecksForm>()
				.RegisterType<ISellView, SellForm>()
				.RegisterType<IReportingService, FastReportService>()
				.RegisterType<IConfirmation, Confirmation>(new ContainerControlledLifetimeManager());

			return cnt;
		}

		private static void InitializeDatabase()
		{
			Database.SetInitializer(new StoreInitializer());
		}
	}
}
